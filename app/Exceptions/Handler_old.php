<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof ErrorException)
        {
           $result['code'] = 401;
            $result['error'] = true;
            $result['message'] = 'Unauthenticated';
            return response()->json($result);
        }
        else if ($exception instanceof NotFoundHttpException)
        {
            // $result['code'] = 404;
            // $result['error'] = true;
            // $result['message'] = 'API Not Found';
            // return response()->json($result);
        }
        else if ($exception instanceof MethodNotAllowedHttpException)
        {
            $result['code'] = 405;
            $result['error'] = true;
            $result['message'] = 'Method Not Allowed';
            return response()->json($result);
        }
        /*else
        {
            $data = [];
             return \Response::make(['status'=>'error','message'=>$exception->getMessage(),'data'=>$data,'error_code'=>'401'], 401);
        }*/
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            $result['code'] = 401;
            $result['error'] = true;
            $result['message'] = 'Unauthenticated';
            return response()->json($result);
        }
        else
        {
            $result['code'] = 401;
            $result['error'] = true;
            $result['message'] = 'Unauthenticated';
            return response()->json($result);
        }
    }
}
