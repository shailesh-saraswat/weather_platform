<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


class Chatmessages implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userId;
    public $userInfo;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     //
    // }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    // public function broadcastOn()
    // {
    //     return new PrivateChannel('channel-name');
    // }

    public function __construct($userId, $userInfo)
    {
        $this->userId = $userId;
        $this->userInfo = $userInfo;
    }
    
    public function broadcastOn()
    {
        return ["chat-channel"];
    }
    // public function broadcastOn()
    // {
    //     return new PrivateChannel('user'.$this->userId->id);
    // }

    public function broadcastAs()
    {
        return "chat-event";
    }
}
