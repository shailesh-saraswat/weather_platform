<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\precipitation;
use App\weathervote as votemodel;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Weathervotenextday extends Component
{
    public $is_temp;

    public $temp_value;

    public $precipitation_id;

    public $precipitation;
    // protected $rules = [
    //     'temp' => 'required',
    //     'temp_value' => 'required',
    //     'precipitation' => 'required',
    // ];
    private function resetInput()
    {
        $this->is_temp = null;
        $this->temp_value = null;
        $this->precipitation_id = null;
    }

    public function mount()
    {
        $this->precipitation=precipitation::get();
    }
   
    public function submit() {
        $user=Auth::user();
        if($user!=null){
        $validatedData = $this->validate( [
            'is_temp' => 'required',
            'temp_value' => 'required',
            'precipitation_id' => 'required',
        ],
        [
            'is_temp.required' => ' The is Temprature field is required.',
            'temp_value.required' => ' The is Temprature value is required.',
            'precipitation_id.required' => ' The Precipitation field is required. '
        ]);
       

        votemodel::create([
            'is_temp'=>$this->is_temp,
            'temp_value'=>$this->temp_value,
            'precipitation_id'=>$this->precipitation_id,
            'user_id'=>$user->id,
            'weatherdate'=>Carbon::now()->addDay(2)

        ]);
        session()->flash('message', 'Voted successfully.');
        $this->resetInput();
    }else{
        redirect('/login'); 
    }
        
    }
    public function render()
    {
        return view('livewire.weathervotenextday');
    }
}
