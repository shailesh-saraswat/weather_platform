<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ChatList extends Component
{
    public $username;
    public $messages;
    protected $ultimoId;
        
    protected $listeners = ['messagereceived', 'changedusername'];
    
    public function mount()
    {
        $ultimoId = 0;
        $this->messages = [];                       
        
        $this->username = request()->query('username', $this->username) ?? "";                   
    }

    public function  messagereceived($data)
    {        
        $this->actualizarmessages($data);
    }

    public function changedusername($username)
    {
        $this->username = $username;
    }

    public function actualizarmessages($data)
    {     
                  
        if($this->username != "")
        {
            // El contenido de la Push
            //$data = \json_decode(\json_encode($data));
            
            $messages = \App\Chat::orderBy("created_at", "desc")->take(5)->get();
            //$this->messages = [];            

            foreach($messages as $message)
            {
                if($this->ultimoId < $message->id)
                {
                    $this->ultimoId = $message->id;
                    
                    $item = [
                        "id" => $message->id,
                        "username" => $message->username,
                        "message" => $message->message,
                        "recibido" => ($message->username != $this->username),
                        "fecha" => $message->created_at->diffForHumans()
                    ];
    
                    array_unshift($this->messages, $item);                
                    //array_push($this->messages, $item);                
                }
                
            }

            if(count($this->messages) > 5)
            {
                array_pop($this->messages);
            }
        }
        else
        {            
            $this->emit('requestusername');
        }
    }

    public function resetmessages()
    {
        $this->messages = [];
        $this->actualizarmessages();
    }

    public function dydrate()
    {
        if($this->username == "")
        {
            // Le pedimos el uisuario al otro componente
            $this->emit('requestusername');
        }
    }

    public function render()
    {
        return view('livewire.chat-list');
    }
}
