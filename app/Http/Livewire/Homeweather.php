<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Homeweather extends Component
{
    // protected $listeners = [
    //     'set:latitude-longitude' => 'setLatitudeLongitude'
    // ];
        public $latitude;
        public $longitude;
        public $weather_result;
        
        // protected $listeners = ['setLatitudeLongitude' => 'showlatlong'];
        protected $listeners = [
            'getLatitudeForInput'
       ];
        public function getLatitudeForInput($val1, $val2) 
        {
            $this->latitude = $val1;
            $this->longitude = $val2;
        //    echo $latitude; die;
        $weatherkey=env('WEATHER_KEY');
        $url = "http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=$weatherkey&q=$this->latitude,$this->longitude&language=en&details=true";
// dd($url);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);

        $json_response = json_decode($resp);

        $location_key= isset($json_response->Key)?$json_response->Key:'0';
// echo $location_key; die;
// dd($json_response);
        curl_close($curl);

        $url = "http://dataservice.accuweather.com/currentconditions/v1/$location_key?apikey=$weatherkey&details=true";
            
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        $weatherreport = json_decode($resp);
        if(isset($weatherreport->Code))
        $this->weather_result =  '';
        else
        $this->weather_result =  $weatherreport;
        // echo '<pre>';
        //    print_r( json_decode($resp)); die;
        curl_close($curl);

        }
       
    public function render()
    {
        return view('livewire.homeweather');
    }
}
