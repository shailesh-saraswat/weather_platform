<?php

namespace App\Http\Livewire;

use Livewire\Component;
// use App\User;
// use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\VerifyOtp;
// use App\Mail\ForgotOtp;

class Forgetpasslayout extends Component
{

    public $email;

    public $user_type;

  

    private function resetInput()
    {
        $this->email = null;
        $this->user_type = null;
        
    }
   
   

    public function render()
    {
        return view('livewire.forgetpasslayout');
    }

   
}
