<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Events\Chatmessages;
use Illuminate\Support\Facades\Auth;


class ChatForm extends Component
{
    // Estas propiedades son publicas
    // y se pueden utilizar directamente desde la vista
    public $username;
    public $message;

    // Generar datos para pruebas
    // private $faker;
    
    // Mantenemos estos valores actualizados en 
    // la barra de direcciones...
    // Ej.: https://your-app.com/?username=Pedro
    // protected $updatesQueryString = ['username'];   
    
    // Eventos Recibidos
    protected $listeners = ['requestusername'];

    // Cuando se Inicia el Componente (antes de Render)
    public function mount()
    {                
        // Instanciamos Faker
        // $this->faker = \Faker\Factory::create();       

        // Obtenemos el valor de username de la barra de direcciones
        // si no existe, generamos uno con Faker
        $this->username =Auth::user()->name; //request()->query('username', $this->username) ?? $this->faker->name;                         

        // Generamos el primer texto de prueba
        $this->message ='hello me'; //$this->faker->realtext(20);
    }
    
    // Cuando el otro componente nos solicitan el username    
    public function requestusername()
    {
        // Lo emitimos por evento
        $this->emit('changedusername', $this->username);
    }

    // Cuando actualizamos el nombre de username
    public function updatedusername()
    {
        // Notificamos al otro componente el cambio
        $this->emit('changedusername', $this->username);
    }

    // Se produce cuando se actualiza cualquier dato por Binding
    public function updated($field)
    {
        // Solo validamos el campo que genera el update
        $validatedData = $this->validateOnly($field, [
            'username' => 'required',
            'message' => 'required',
        ]);
    }

    public function sendmessage()
    {      
        $user = Auth::user();          
        $validatedData = $this->validate([
          
            'message' => 'required',
        ]);
        $this->username = $user->name;
        // Guardamos el message en la BBDD
        \App\Chat::create([
            "username" => $this->username,
            "message" => $this->message
        ]);
        // dd($this->message);
        // Generamos el evento para Pusher
        // Enviamos en la "push" el username y message (aunque en este ejemplo no lo utilizamos)
        // pero nos vale para comprobar en PusherDebug (y por consola) lo que llega...
        // event(new \App\Events\Chatmessages($this->username, $this->message));
        // event(new \App\Events\Chatmessages($this->username, $this->message));
        event(new Chatmessages($user,  $this->message));
        // Este evento es para que lo reciba el componente
        // por Javascript y muestre el ALERT BOOSTRAP de "enviado"
        $this->emit('successmsg', $this->message);
        

        // Creamos un nuevo texto aleatorio (para el próximo message)
        // $this->faker = \Faker\Factory::create();       
        // $this->message = $this->faker->realtext(20);
    
    }   
    public function render()
    {
        return view('livewire.chat-form');
    }
}
