<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\weathervote as votemodel;
use App\precipitation;



class Weathervote extends Controller
{
    //
    public function weathervoteApi(Request $request){
        try
        {
            $validator = Validator::make($request->all(), [
                'is_temp' => 'required',
                'temp_value' => 'required',
                'precipitation_id' => 'required',
                'weatherdate'=>'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>true,'message'=>$validator->messages()->first(),'code'=>422]);
            }
            else{
                $user = Auth::user();

                $weathervote = new votemodel();
                $weathervote->is_temp = $request->input('is_temp');
                $weathervote->temp_value = $request->input('temp_value');
                $weathervote->precipitation_id = $request->input('precipitation_id');
                $weathervote->user_id = $user->id;
                $weathervote->weatherdate = $request->input('weatherdate');


                // $weathervote = weathervote::create([
                //     'is_temp'=>$request->input('is_temp'),
                //     'temp_value'=> $request->input('temp_value'),
                //     'precipitation_id'=>$request->input('precipitation_id'),
                //     'user_id'=>$user->id
        
                // ]);
                // $user->status = '2';
                // $user->password = bcrypt($request->input('password'));
                if($weathervote->save())
                {
                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['message'] = "Voted Successfully.";
                    $result['data'] = $weathervote;
                    return response()->json($result);
                }
                else
                {
                    $result['code'] = 500;
                    $result['error'] = true;
                    $result['message'] = 'Something Went wrong please try again';
                    return response()->json($result);
                }
            }
        }
        catch(QueryException $ex){ 
            $result['code'] = 400;
            $result['error'] = true;
            $result['message'] = $ex->getMessage();
            return response()->json($result);
        }
    }

    public function precipitations(Request $request)
    {
        try
        {
            $precipitation=precipitation::all();
           
                if($precipitation)
                {
                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['message'] = "precipitation Successfully.";
                    $result['data'] = $precipitation;
                    return response()->json($result);
                }
                else
                {
                    $result['code'] = 500;
                    $result['error'] = true;
                    $result['message'] = 'Something Went wrong please try again';
                    return response()->json($result);
                }
            
        }
        catch(QueryException $ex){ 
            $result['code'] = 400;
            $result['error'] = true;
            $result['message'] = $ex->getMessage();
            return response()->json($result);
        }
    }



}
