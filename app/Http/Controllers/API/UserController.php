<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Pages;
use Validator;
use Response;
use Session;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyOtp;
use App\Mail\ForgotOtp;


class UserController extends Controller
{
    public function register(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:3',
                'email'=>'required|email|unique:users',
                'password' => 'required|min:2|max:16',
                'city' => 'required',
                'user_type' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>true,'message'=>$validator->messages()->first(),'code'=>422]);
            }
            else{
                if($request->has('email') && $request->email!='')
                {
                    if (!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
                        $result['code'] = 400;
                        $result['error'] = true;
                        $result['message'] = "Enter proper email address.";
                        return response()->json($result);
                    }
                }
                $user = new User();
                if($request->user_type=='3')
                {
                    if(!$request->has('meterologist_docs'))
                    {
                        return response()->json(['error'=>true,'message'=>'Please choose document for verification','code'=>422]);
                    }
                    if(request()->meterologist_docs)
                    {
                        $imageName = time().'.'.request()->meterologist_docs->getClientOriginalExtension();
                        request()->meterologist_docs->move(public_path('uploads/meterologist_docs'), $imageName);
                        $user->meterologist_docs=$imageName;
                    }
                }
                $user->name = $request->input('name');
                $user->email = $request->input('email');
                $user->city = $request->input('city');
                $user->user_type = $request->input('user_type');
                $user->status = '2';
                $user->password = bcrypt($request->input('password'));
                if($user->save())
                {
                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['message'] = "Registered Successfully.";
                    $result['data'] = $user;
                    return response()->json($result);
                }
                else
                {
                    $result['code'] = 500;
                    $result['error'] = true;
                    $result['message'] = 'Something Went wrong please try again';
                    return response()->json($result);
                }
            }
        }
        catch(QueryException $ex){ 
            $result['code'] = 400;
            $result['error'] = true;
            $result['message'] = $ex->getMessage();
            return response()->json($result);
        }
    }

    public function get_static_content(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'user_type' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>true,'message'=>$validator->messages()->first(),'code'=>422]);
            }
            $static_data['terms_and_conditions'] = Pages::where('user_type',$request->user_type)->where('page_type','terms_service')->first()->content;
            $static_data['privacy_policy'] = Pages::where('user_type',$request->user_type)->where('page_type','privacy_policy')->first()->content;
            $result['code'] = 200;
            $result['error'] = false;
            $result['message'] = "Static Data.";
            $result['data'] = $static_data;
            return response()->json($result);

        }
        catch(QueryException $ex){ 
            $result['code'] = 500;
            $result['error'] = true;
            $result['message'] = $ex->getMessage();
            return response()->json($result);
        }

    }

    public function email_verfiy(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'user_type' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>true,'message'=>$validator->messages()->first(),'code'=>422]);
            }

            $user_data=User::where(['email'=>$request->input('email')])->where(['user_type'=>$request->input('user_type')])->first();
            // dd($user_data);
            if($user_data)
            {
                $rndno = rand(1000, 9999); //OTP generate
                User::where('email',$user_data->email)->update(['verify_otp'=>$rndno]);
                Mail::to($user_data->email)->send(new VerifyOtp($user_data->name,$rndno));
                $result['code'] = 200;
                $result['error'] = false;
                $result['message'] = 'Otp sent to registered email-id';
                $result['otp'] = $rndno;
                return response()->json($result);
            }
            else
            {
                $result['code'] = 400;
                $result['error'] = true;
                $result['message'] = 'Please try with registered email id';
                return response()->json($result);
            }
        }
        catch(QueryException $ex){ 
            $result['code'] = 500;
            $result['error'] = true;
            $result['message'] = $ex->getMessage();
            return response()->json($result);
        }

    }

    public function verify_otp(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                    'otp'      => 'required|numeric',
                    'email'         => 'required|email',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>true,'message'=>$validator->messages()->first(),'code'=>422]);
            }
            else
            {
                $check=User::where(['email'=>$request->input('email')])->where(['verify_otp'=>$request->input('otp')])->first();
                if($check)
                {
                    User::where('email',$request->email)->update(['status'=>'1']);
                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['message'] = 'Otp verified successfully';
                    $user = Auth::loginUsingId($check->id);
                    Auth::user()->AauthAcessToken()->delete();
                    $token =  $user->createToken('MyApp')->accessToken;
                    $result['token'] = $token;
                    return response()->json($result);
                } 
                else
                {
                    $result['code'] = 400;
                    $result['error'] = true;
                    $result['message'] = 'Please enter valid OTP';
                    return response()->json($result);
                }
            }
        }
        catch(QueryException $ex){ 
            $result['code'] = 500;
            $result['error'] = true;
            $result['message'] = $ex->getMessage();
            return response()->json($result);
        }
    }

    public function login(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                    'email'         => 'required|email',
                    'password'         => 'required',
                    'user_type'         => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>true,'message'=>$validator->messages()->first(),'code'=>422]);
            }
            else
            {
                if(Auth::attempt(['email' => request('email'), 'password' => request('password'),'user_type'=>request('user_type') ,'status'=>1]))
                {
                    $user = Auth::user();
                    if($request->has('device_type') && $request->device_type!='')
                    {
                        $user->device_type = $request->device_type;
                        $user->save();
                    }
                    if($request->has('device_token') && $request->device_token!='')
                    {
                        $user->device_token = $request->device_token;
                        $user->save();
                    }
                    $message = 'Logged in successfully';
                    $base_url = $_ENV['APP_URL']."/public/uploads/profiles/";
                    $details = User::select('*',DB::raw("'$base_url' as base_url"))->where('users.id',$user->id)->first();
                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['message'] = 'Logged in successfully';
                    // $token = "";
                    // if(Auth::user()->status=='1')
                    // {
                        Auth::user()->AauthAcessToken()->delete();
                        $token =  $user->createToken('MyApp')->accessToken;
                    // }
                    $result['token'] = $token;
                    $result['data'] = $details;
                    return response()->json($result);
                }
                else
                {
                    $result['code'] = 400;
                    $result['error'] = true;
                    $result['message'] = 'Invalid Credentials Or Your account is in-active';
                    return response()->json($result);
                }
            }
        }
        catch(QueryException $ex){ 
            $result['code'] = 500;
            $result['error'] = true;
            $result['message'] = $ex->getMessage();
            return response()->json($result);
        }
    }

    public function forgot_password(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                    'email'         => 'required|email',
                    'user_type'         => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>true,'message'=>$validator->messages()->first(),'code'=>422]);
            }
            else
            {
                $user_data=User::where('email',$request->email)->where('user_type',$request->user_type)->first();
                if($user_data)
                {
                    $rndno = rand(1000, 9999); //OTP generate
                    User::where('email',$user_data->email)->update(['otp'=>$rndno]);
                    Mail::to($user_data->email)->send(new ForgotOtp($user_data->name,$rndno));
                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['message'] = 'Otp sent successfully on registered email-id';
                    $result['otp'] = $rndno;
                    return response()->json($result);
                } 
                else
                {
                    $result['code'] = 400;
                    $result['error'] = true;
                    $result['message'] = 'Please try with registered email-id';
                    return response()->json($result);
                }
            }
        }
        catch(QueryException $ex){ 
            $result['code'] = 500;
            $result['error'] = true;
            $result['message'] = $ex->getMessage();
            return response()->json($result);
        }
    }

    public function verify_forgot_otp(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                    'otp'      => 'required|numeric',
                    'email'         => 'required|email',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>true,'message'=>$validator->messages()->first(),'code'=>422]);
            }
            else
            {
                $check=User::where(['email'=>$request->input('email')])->where(['otp'=>$request->input('otp')])->first();
                if($check)
                {
                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['message'] = 'Otp verified successfully';
                    return response()->json($result);
                } 
                else
                {
                    $result['code'] = 400;
                    $result['error'] = true;
                    $result['message'] = 'Please enter valid OTP';
                    return response()->json($result);
                }
            }
        }
        catch(QueryException $ex){ 
            $result['code'] = 500;
            $result['error'] = true;
            $result['message'] = $ex->getMessage();
            return response()->json($result);
        }
    }

    public function reset_password(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                    'email' => 'required|email|max:191',
                    'new_password' => 'required|same:confirm_password',
                    'confirm_password' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>true,'message'=>$validator->messages()->first(),'code'=>422]);
            }
            else
            {
                $user_data=User::where(['email'=>$request->input('email')])->first();
                if($user_data)
                {
                    $password = bcrypt($request->new_password);
                    User::where('email', $request->email)
                            ->update(['password' => $password,'otp'=>null]);
                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['message'] = 'Password reset successfully';
                    return response()->json($result);
                } 
                else
                {
                    $result['code'] = 400;
                    $result['error'] = true;
                    $result['message'] = 'Please try with registered email-id';
                    return response()->json($result);
                }
            }
        }
        catch(QueryException $ex){ 
            $result['code'] = 500;
            $result['error'] = true;
            $result['message'] = $ex->getMessage();
            return response()->json($result);
        }
    }

    public function logout(Request $request)
    {
        try
        {
            $user = Auth::user();
            if(Auth::check()) 
            {
                Auth::user()->AauthAcessToken()->delete();
                $result['code'] = 200;
                $result['error'] = false;
                $result['message'] = 'Logged out successfully';
                return response()->json($result);
            }
            else
            {
                $result['code'] = 401;
                $result['error'] = true;
                $result['message'] = 'Unauthenticated';
                return response()->json($result);
            }
        }
        catch(QueryException $ex){ 
            $result['code'] = 500;
            $result['error'] = true;
            $result['message'] = $ex->getMessage();
            return response()->json($result);
        }
    }
}
