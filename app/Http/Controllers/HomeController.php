<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Response;
use Session;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Cart;
use DB;


class HomeController extends Controller
{
    // public function __construct(){
    //     $this->middleware('auth');
    // }
    public function index()
    {
        
        return view('home');

    }

    public function about_us()
    {
        return view('about_us');
    }

    public function contact_us()
    {
        return view('contact_us');
    }

    public function gallery()
    {
        return view('gallery');
    }

    public function login()
    {
        return view('login');
    }

    public function vote_now()
    {
        return view('vote_now');
    }

    public function privacy_policy()
    {
        return view('privacy_policy');
    }

    public function register()
    {
        return view('register');
    }

    public function term_conditions()
    {
        return view('term_conditions');
    }

}
