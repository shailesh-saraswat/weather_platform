<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\weathervote;
use App\precipitation;
use Validator;
use Response;
use Session;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;


class HomeController extends Controller
{
    public function dashboard(Request $request){
        $user=Auth::user();
        $precipitation=precipitation::get();
// dd($request->all());
        $userip = $request->ip();
        // echo $userip; die;
        // if($request->lat){
        //     echo $request->lat.'-->'.$request->long; die;
        // }
        $weatherkey=env('WEATHER_KEY');
        $url = "http://dataservice.accuweather.com/locations/v1/cities/ipaddress?apikey=$weatherkey&q=$userip&language=en&details=true";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        
        $resp = curl_exec($curl);
       
        $json_response = json_decode($resp);
        
        $location_key= isset($json_response->Key)?$json_response->Key:'0';
        
        curl_close($curl);


        $url = "http://dataservice.accuweather.com/currentconditions/v1/$location_key?apikey=$weatherkey&details=true";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        
        $resp = curl_exec($curl);
        $weather_result = json_decode($resp);
    //    dd($weather_result);
        curl_close($curl);
       
        return view('user/dashboard',compact('user','precipitation'));
    }
    public function getlatlong(Request $request)
    {
        // echo 'ss'; die;
        # get lat long of the user...
        $latrequest=$request->lat;
        $longrequest=$request->long;

// http://dataservice.accuweather.com/locations/v1/cities/geoposition/search

        $weatherkey=env('WEATHER_KEY');
        $url = "http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=$weatherkey&q=$latrequest,$longrequest&language=en&details=true";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);

        $json_response = json_decode($resp);

        $location_key= isset($json_response->Key)?$json_response->Key:'0';
// echo $location_key; die;
        curl_close($curl);
        $url = "http://dataservice.accuweather.com/currentconditions/v1/$location_key?apikey=$weatherkey&details=true";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        
        $resp = curl_exec($curl);
        $weatherreport = json_decode($resp);
    //    dd($weatherreport);
        if(isset($weatherreport->Code))
        $weather_result =  '';
        else
        $weather_result =  $weatherreport;
        curl_close($curl);

        return view('user/weathertemp',compact('weather_result'));

    }

    public function logout(){
        Auth::logout();
        Session::flash ( 'log_message', "Logged out successfully. Please login again" );
        return redirect('/login');
    }

    public function weathervote(Request $request){
        

                    try
                    {
                        $rules=array(
                        'temp' => 'required',
                        'temp_value'=> 'required',
                        'precipitation'=> 'required',

                        );

                    $validator = Validator::make($request->all(), $rules);
                    if ($validator->fails())
                    {
                        return redirect()->back()->withErrors($validator)->withInput();
                    }

                    // if ($request->has('category_id') && $request->category_id != "")
                    // {
                    //     $category = Category::find($request->category_id);
                    // }			
                    // else
                    // {
                        $weathervote = new weathervote();
                    // }
                    //     if(!empty($category))
                    //     {
                        $weathervote->weatherdate =now();
                            
                        $weathervote->is_temp = $request->temp;
                        $weathervote->temp_value = $request->temp_value;
                        $weathervote->precipitation_id = $request->precipitation;

                        $weathervote->save();

                    //     if($request->has('category_id'))
                    //     {
                    //         Session::flash ('category_message', "Category Updated Successfully.");
                    //     }
                    //     else
                    //     {
                            Session::flash ('alert_message', "Category Uploaded successfully.");
                    //     }				
                            return redirect('/user/dashboard');
                    // }
                }
                catch(QueryException $ex)
                { 
                    Session::flash ('alert_message', $ex->getMessage());
                    return redirect('/user/dashboard');
                }
    }


    public function messageschat()
    {
        # code...
        $user=Auth::user();
        return view("home.index",compact('user'));
        
    }

}
?>