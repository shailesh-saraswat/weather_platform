<?php

namespace App\Http\Controllers\Meterologists;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Response;
use Session;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;


class HomeController extends Controller
{
    public function dashboard(){
        return view('meterologists/dashboard');
    }

    public function logout(){
        Auth::logout();
        Session::flash ( 'log_message', "Logged out successfully. Please login again" );
        return redirect('/login');
    }
}
?>