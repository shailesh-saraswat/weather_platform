<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Response;
use Session;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Cart;
use DB;
use App\Mail\VerifyOtp;
use App\Mail\ForgotOtp;


class CommonController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function register(Request $request)
    {
        //print_r($request->all()); die;
        try {
            $rules = array(
                'name' => 'required',
                'email' => 'required|email|unique:users,email,',
                'password' =>  ['required', 
                'min:6', 
                'regex:/^(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z])(?=\D*\d)(?=[^!@?]*[!@?]).{10,}$/'],
                'city' => 'required',
                'confirm_password' => 'required|same:password'           // required and has to match the password field
            );

            $customMessages = array(
                'password.required' => 'The password is not strong.',
            );
            $validator = Validator::make($request->all(), $rules,$customMessages);

           
            
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $users = new User();
                $users->name = $request->name;
                $users->user_type = $request->optradio;
                $users->email = $request->email;
                if ($request->has('password') && $request->password != '') {
                    $users->password = bcrypt($request->password);
                }
                if ($users->save()) {
                    //Mail::to($users->email)->send(new UserReg($users));
                }
                Session::flash('reg_message', "Registered successfully.");
                return redirect('/login');
            }
        } catch (QueryException $ex) {
            print_r($ex->getMessage()); die;
            Session::flash('reg_message', $ex->getMessage());
            return redirect('/register');
        }
    }

    public function do_login(Request $request)
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password'), 'user_type' => request('user_type')]))
        {
            $user_type = Auth::user()->user_type;
            if($user_type=='2')
            {
                return redirect('/user/dashboard');
            }
            else
            {
                return redirect('/meterologists/dashboard');
            }
        } else {
            Session::flash('message', "Invalid Credentials , Please try again.");
            return Redirect::back();
        }
    }


    public function forgot_password(Request $request)
    {
        return view('forgetpassword');
    }

    public function sendotp(Request $request)
    {
        try
        {
            // dd($request->all());
            $validator = Validator::make($request->all(), [
                    'email'         => 'required|email',
                    'user_type'         => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>true,'message'=>$validator->messages()->first(),'code'=>422]);
            }
            else
            {
                $user_data=User::where('email',$request->email)->where('user_type',$request->user_type)->first();
            //    dd($user_data);
                if($user_data)
                {
                    $email = $user_data->email;
                    $rndno = rand(1000, 9999); //OTP generate
                    User::where('email',$user_data->email)->update(['otp'=>$rndno]);
                    Mail::to($user_data->email)->send(new ForgotOtp($user_data->name,$rndno));
                    return view('verifyotp',compact('email'));
                    // return redirect('/verifyotp');
                 
                } 
                else
                {
                    Session::flash('message', "Please try with registered email-id.");
                    return Redirect::back();
                   
                }
            }
        }
        catch(QueryException $ex){ 
            Session::flash('message', "Invalid Credentials , Please try again.");
            return Redirect::back();
        }
    }

    public function verifyotp(Request $request)
    {
        // dd($request->all());
            $validator = Validator::make($request->all(), [
                    'verifyotp'      => 'required|numeric',
                    'email'         => 'required|email',
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>true,'message'=>$validator->messages()->first(),'code'=>422]);
            }
            else
            {
                $check=User::where(['email'=>$request->input('email')])->where(['otp'=>$request->input('verifyotp')])->first();
                if($check)
                {
                    // $result['code'] = 200;
                    // $result['error'] = false;
                    // $result['message'] = 'Otp verified successfully';
                    // return response()->json($result);
                    $email = $request->input('email');
                    return view('resetpassword',compact('email'));
                } 
                else
                {
                    // $result['code'] = 400;
                    // $result['error'] = true;
                    // $result['message'] = 'Please enter valid OTP';
                    // return response()->json($result);
                    Session::flash('reg_message', "Please enter valid OTP");
                    return redirect('/forgot_password');
                }
            }
    }


    public function reset_password(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                    'email' => 'required|email|max:191',
                    'new_password' => 'required|same:confirm_password',
                    'confirm_password' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>true,'message'=>$validator->messages()->first(),'code'=>422]);
            }
            else
            {
                $user_data=User::where(['email'=>$request->input('email')])->first();
                if($user_data)
                {
                    $password = bcrypt($request->new_password);
                    User::where('email', $request->email)
                            ->update(['password' => $password,'otp'=>null]);
                    // $result['code'] = 200;
                    // $result['error'] = false;
                    // $result['message'] = 'Password reset successfully';
                    // return response()->json($result);
                    Session::flash('reg_message', "Password reset successfully.");
                    return redirect('/login');
                } 
                else
                {
                    // $result['code'] = 400;
                    // $result['error'] = true;
                    // $result['message'] = 'Please try with registered email-id';
                    Session::flash('reg_message', "OTP is expired.");
                    return redirect('/forgot_password');
                    // return response()->json($result);
                }
            }
        }
        catch(QueryException $ex){ 
            // $result['code'] = 500;
            // $result['error'] = true;
            // $result['message'] = $ex->getMessage();
            // return response()->json($result);
            Session::flash('reg_message', $ex->getMessage());
            return redirect('/forgot_password');
        }
    }


}
