<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Orders;
use Validator;
use Response;
use Session;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;


class CommonController extends Controller
{
    public function index(){
        if (Auth::check())
        {
            return redirect('/admin/dashboard');
        }
        return view('admin_login');
    }

    public function login(Request $request)
    {
        if(Auth::guard('admin')->attempt(['email' => request('email'), 'password' => request('password'),'user_type'=>'1']))
                {
            session ( [
                    'name' => $request->get ( 'email' )
            ] );
            return Redirect("admin/dashboard");
        } else {
            Session::flash ( 'message', "Invalid Credentials , Please try again." );
            return Redirect::back ();
        }
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        Session::flash ( 'log_message', "Logged out successfully. Please login again" );
        return redirect('/admin');
    }

    public function dashboard()
    {
        $user_count = User::where('deleted','0')->where('user_type','2')->count();
        $orders = Orders::orderBy('id','DESC')->limit(10)->get();
        $new_orders = Orders::where('order_status_id','1')->count();
        $completed_orders = Orders::where('order_status_id','5')->count();
        return view('admin/dashboard',compact('user_count','orders','new_orders','completed_orders'));
    }

    public function home()
    {
        return view('admin/home');
    }
}
