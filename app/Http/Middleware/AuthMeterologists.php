<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\Session;

class AuthMeterologists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() && Auth::user()->user_type!='3') {
            Auth::logout();
            Session::flash ( 'message', "Please Login again." );
            return redirect('/login');
        }
        else if (!Auth::user()) {
            Session::flash ( 'message', "Please Login again." );
            return redirect('/login');
        }

        return $next($request);
    }
}
