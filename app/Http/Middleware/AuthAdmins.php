<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\Session;

class AuthAdmins
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //print_r(Auth::user()); die; 
        //echo Auth::user()->user_type; die;
        if (Auth::guard('admin')->user() && Auth::guard('admin')->user()->user_type!='1') {
            Auth::guard('admin')->logout();
            Session::flash ( 'message', "Please Login again." );
            return redirect('/admin');
        }
        else if (!Auth::guard('admin')->user()) {
            Session::flash ( 'message', "Please Login again." );
            return redirect('/admin');
        }

        return $next($request);
    }
}
