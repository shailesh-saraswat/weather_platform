<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\Session;

class AuthCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //print_r(Auth::user()); die; 
        //echo Auth::user()->user_type; die;
        if (Auth::user() && Auth::user()->user_type!='2') {
            Auth::logout();
            Session::flash ( 'message', "Please Login again." );
            return redirect('/login');
        }
        else if (!Auth::user()) {
            Session::flash ( 'message', "Please Login again." );
            return redirect('/login');
        }

        return $next($request);
    }
}
