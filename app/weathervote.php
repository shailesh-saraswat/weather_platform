<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class weathervote extends Model
{
    
       /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'weatherdate', 'is_temp', 'temp_value','precipitation_id','user_id'
    ];
}
