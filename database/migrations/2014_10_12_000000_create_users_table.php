<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('user_type', ['1','2','3'])->default('1')->comment('1=>Admin,2=>User,3=>Meterologists');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->bigInteger('phone')->nullable();
            $table->string('city')->nullable();
            $table->text('profile_image')->nullable();
            $table->text('meterologist_docs')->nullable();
            $table->string('otp',10)->nullable();
            $table->string('verify_otp',10)->nullable();
            $table->rememberToken();
            $table->enum('status', ['1','2'])->default('1')->comment('1=>Active,2=>Inactive');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
