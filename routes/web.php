<?php

 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('changepassword', function() {
//     $user = App\User::where('email', 'shailesh.saraswat@webmobril.com')->first();
//     $user->password = Hash::make('123456');
//     $user->save();
//     echo 'Password changed successfully.';
// });
Auth::routes();
Route::get('/', 'HomeController@index')->name('frontHome');
Route::get('/admin', 'Admin\CommonController@index')->name('Home');
Route::post('/admin_login', 'Admin\CommonController@login')->name('AdminLogin');

/*live wire call*/
// Route::livewire('forgetpassword', 'forget-password')->section('body');
/*End of livewire call */

Route::get('/about_us', 'HomeController@about_us')->name('aboutUs');
Route::get('/contact_us', 'HomeController@contact_us')->name('contactUs');
Route::get('/gallery', 'HomeController@gallery')->name('Gallery');
Route::get('/login', 'HomeController@login')->name('Login');
Route::get('/vote_now', 'HomeController@vote_now')->name('Vote');
Route::get('/privacy_policy', 'HomeController@privacy_policy')->name('privacyPolicy');
Route::get('/register', 'HomeController@register')->name('Register');
Route::get('/term_conditions', 'HomeController@term_conditions')->name('termsConditions');

Route::get('/forgot_password', 'CommonController@forgot_password')->name('forgotPassword');
Route::post('/forgot_password', 'CommonController@forgotPass')->name('forgotPass');
Route::post('/sendotp', 'CommonController@sendotp')->name('sendotp');
Route::post('/verifyotp', 'CommonController@verifyotp')->name('verifyotp');
Route::post('/reset_password', 'CommonController@reset_password')->name('reset_password');




Route::post('password', 'CommonController@resetPassword')->name('resetPassword');
Route::get('reset/password/{token}', 'CommonController@reset_password_page')->name('reset_password_page');
Route::post('do_login', 'CommonController@do_login')->name('doLogin');
Route::get('logout', 'CommonController@logout')->name('userLogout');
Route::post('save_user', 'CommonController@register')->name('saveUser');

Route::post('latlong','User\HomeController@getlatlong')->name('latlong');

Route::group(['middleware' => 'auth.admin','prefix' => 'admin'], function()
{
	Route::get('dashboard', 'HomeController@dashboard')->name('adminDashboard');
	Route::get('logout', 'HomeController@logout')->name('adminLogout');

});

Route::group(['middleware' => 'auth.customer','prefix' => 'user'], function()
{
	Route::get('dashboard', 'User\HomeController@dashboard')->name('userDashboard');
	Route::get('logout', 'User\HomeController@logout')->name('userLogout');

	Route::post('weathervote','User\HomeController@weathervote')->name('weathervote');


	Route::get('messages', 'User\HomeController@messageschat')->name('messageschat');

	// Route::post('/submit');
	

});

Route::group(['middleware' => 'auth.meterologists','prefix' => 'meterologists'], function()
{
	Route::get('dashboard', 'Meterologists\HomeController@dashboard')->name('meterologistsDashboard');
	Route::get('logout', 'Meterologists\HomeController@logout')->name('meterologistsLogout');

});

