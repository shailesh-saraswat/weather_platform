<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'API\UserController@login')->name('userLogin');
Route::post('register', 'API\UserController@register')->name('userRegister');
Route::post('email_verfiy', 'API\UserController@email_verfiy')->name('email_verfiy');
Route::post('verify_otp', 'API\UserController@verify_otp')->name('verify_otp');
Route::post('get_static_content', 'API\UserController@get_static_content')->name('staticContent');
Route::post('forgot_password','API\UserController@forgot_password');
Route::post('verify_forgot_otp','API\UserController@verify_forgot_otp');
Route::post('reset_password','API\UserController@reset_password');

Route::group(['middleware' => ['auth:api']], function () {
	Route::get('logout','API\UserController@logout');

	Route::post('vote','API\Weathervote@weathervoteApi');

	Route::get('precipitations','API\Weathervote@precipitations');


});
