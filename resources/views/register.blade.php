@extends('layouts/front')
@section('pagetitle')
Register
@stop
@section('content')
<div class="container inside-top-mar">
	<div class="registerWrap">
		<div class="resLeft">
			<img src="{{url('assets/front/images/registImg.png')}}">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner">
				<div class="item active">
					<div class="resUserReview">
						<h4> John Smith </h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>

				<div class="item">
					<div class="resUserReview">
						<h4> Steve Brandon </h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>

				<div class="item">
					<div class="resUserReview">
						<h4> Lina Atra </h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
				</div>
			  </div>
			</div>			
		</div>
		
		<div class="resRight">
			<h4 class="mt-0 logs_title">Create An <span class="resSpan">Account</span></h4>
			<form action="{{route('saveUser')}}" method="post" name="registerForm" id="registerForm">
				@if(Session::has('reg_message'))
			      <div class="alert alert-info alert-dismissible page_alert">
			        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			        <strong>{{ Session::get('reg_message') }}</strong>
			      </div>
			    @endif
			    <div class="alert alert-danger alert-dismissible custom_alert" style="display: none;">
                	<span class="alert_message"></span>
				</div>
				@if ($errors->any())
					<div class="alert alert-danger alert-dismissible page_alert">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
						</ul>
					</div>
				@endif
				@csrf
				<div class="userProfChoose">
				    <label class="radio-inline">
						<input type="radio" name="optradio" checked value="2">User
					</label>
					<label class="radio-inline">
						<input type="radio" name="optradio" value="3">Meterologists
					</label>
				</div>
				<div class="form-group">
					<label>Name*</label>
					<input type="text" class="form-control" id="name" name="name" placeholder="Enter your name">
				</div>			
				<div class="form-group">
					<label>Email Address*</label>
					<input type="email" class="form-control" id="email" name="email" placeholder="Enter your email">
				</div>			
				<div class="form-group">
					<label>Password*</label>
					<input type="password" class="form-control" id="password" name="password" placeholder="Enter your password">
				</div>
				<div class="form-group">
					<label>Confirm Password*</label>
					<input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Enter confirm password">
				</div>
				<div class="form-group">
					<label>Select City</label>
					<select name="city" id="city">
						<option value="barcelona" selected>Barcelona</option>
						<option value="madrid">Madrid</option>
					</select>
				</div>
				<label class="checkbox-inline resCheckboxLabel">
				  <input type="checkbox" value="1" id="terms" class="terms"><span>Accept Terms and Condition </span>
				</label>
				<a href="javascript:void(0);" class="btn_apply reg_btn">Sign Up</a>
				
				<div class="resQues"> Already have an account <a href="{{route('Login')}}"> Login? </a>	</div>
				
			</form>
		</div>
	</div>	
</div>
@endsection
@section('js')
<script type="text/javascript">
	$('.reg_btn').on('click',function(){
    var name          = $('#name').val().trim();
    var email         = $('#email').val().trim();
    var password      = $('#password').val().trim();
    var confirm_password      = $('#confirm_password').val().trim();
    var error_message = "";
    if(name=='')
    {
      error_message = "Please Enter Name";
    }
    else if(name!='' && name.length > 35)
    {
      error_message = "Please Enter Name Less than 35 characters";
    }
    else if(email=="")
    {
      error_message = "Please Enter Email";
    }
    else if(!IsEmail(email))
    {
      error_message = "Email must be a valid email address";
    }
    else if(password=="")
    {
      error_message = "Please Enter Password";
    }
    else if(confirm_password=="")
    {
      error_message = "Please Enter Confirm Password";
    }
    else if(password!=confirm_password)
    {
      error_message = "Password Not Matched";
    }
    else if($("#terms").prop('checked') != true){
	    error_message = "Please Accept and Terms Condition";
	}
    //alert(error_message);
    if(error_message!='')
    {
      $('.custom_alert').css('display','block');
      $('.alert_message').text(error_message);
      return false;
    }
    else
    {
      $('#registerForm').submit();
    }
  });
</script>
@endsection