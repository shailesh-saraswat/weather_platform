@extends('layouts/front')
@section('pagetitle')
    Login
@stop
@section('content')
    <div class="container inside-top-mar">
        <div class="registerWrap">
            <div class="resLeft">
                <img src="{{ url('assets/front/images/registImg.png') }}">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="resUserReview">
                                <h4> John Smith </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                    ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="resUserReview">
                                <h4> Steve Brandon </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                    ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="resUserReview">
                                <h4> Lina Atra </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                    ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="resRight">
                <h4 class="mt-0 logs_title">Sing In <span class="resSpan">Account</span></h4>
                <form action="{{ route('doLogin') }}" method="post" id="loginForm" name="loginForm">
                    @csrf
                    @if (Session::has('reg_message'))
                        <div class="alert alert-info alert-dismissible page_alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('reg_message') }}</strong>
                        </div>
                    @endif
                    @if (Session::has('message'))
                        <p class="alert alert-danger" id="alert_box">{{ Session::get('message') }}</p>
                    @endif
                    @if (Session::has('log_message'))
                        <p class="alert alert-success" id="alert_box">{{ Session::get('log_message') }}</p>
                    @endif
                    <div class="alert alert-danger alert-dismissible custom_alert" style="display: none;">
                        <span class="alert_message"></span>
                    </div>
                    <div class="userProfChoose">
                        <label class="radio-inline">
                            <input type="radio" name="user_type" value="2" checked>User
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="user_type" value="3">Meterologists
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Email Address*</label>
                        <input type="email" id="email" name="email" class="form-control"
                            placeholder="Please enter your email">
                    </div>
                    <div class="form-group">
                        <label>Password*</label>
                        <input type="password" id="password" name="password" class="form-control"
                            placeholder="Please enter your password">
                    </div>
                    <div class="frtPwd">
                        <a href="{{ url('forgot_password') }}"><span>Forgot Password?</span> </a>
                        {{-- @livewire('forget-password') --}}
                    </div>
                    <a href=" javascript:void(0)" class="btn_apply login_btn">Sign In</a>
                    <div class="resQues"> Don’t have an account <a href="{{ route('Register') }}"> Sign Up?
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @livewireScripts()

@endsection
@section('js')
    <script type="text/javascript">
        //alert(lang);
        $('.login_btn').on('click', function() {
            var email = $('#email').val();
            var password = $('#password').val();
            var error_message = "";
            if (email == "") {
                error_message = "Please Enter Email";
            } else if (!IsEmail(email)) {
                error_message = "Email must be a valid email address";
            } else if (password == "") {
                error_message = "Please Enter Password";
            }
            //alert(error_message); return false;
            if (error_message != '') {
                $('.custom_alert').css('display', 'block');
                $('.alert_message').text(error_message);
                return false;
            } else {
                $('#loginForm').submit();
            }
        });
    </script>
    <script type="text/javascript">
        const myForm = document.getElementById("loginForm");
        document.querySelector(".submit").addEventListener("click", function() {
            myForm.submit();
        });
    </script> -->
@endsection
