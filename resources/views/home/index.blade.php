{{-- @extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="mx-3 my-3">

                <div class="row">
                    <div class="col-md-6">
                        @livewire("chat-form")
                    </div>
                    <div class="col-md-6">
                        @livewire("chat-list")
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection --}}


@extends('layouts/user_front')
@section('pagetitle')
    Dashboard
@stop
@section('content')
    <div class="container m-auto">

        <h1 class="font-semibold lg:mb-6 mb-3 text-2xl"> Messages</h1>

        <div
            class="lg:flex lg:shadow lg:bg-white lg:space-y-0 space-y-8 rounded-md lg:-mx-0 -mx-5 overflow-hidden lg:dark:bg-gray-800">
            <!-- left message-->
            <div class="lg:w-4/12 bg-white border-r overflow-hidden dark:bg-gray-800 dark:border-gray-600">

                <!-- search-->
                <div class="border-b px-4 py-4 dark:border-gray-600">
                    <div class="bg-gray-100 input-with-icon rounded-md dark:bg-gray-700">
                        <input id="autocomplete-input" type="text" placeholder="Search"
                            class="bg-transparent max-h-10 shadow-none">
                        <i class="icon-material-outline-search"></i>
                    </div>
                </div>

                <!-- user list-->
                <div class="pb-16 w-full">
                    <ul class="dark:text-gray-100">
                        <li>
                            <a href="#"
                                class="block flex items-center py-3 px-4 space-x-3 hover:bg-gray-100 dark:hover:bg-gray-700">
                                <div class="w-12 h-12 rounded-full relative flex-shrink-0">
                                    <img src="assets/images/avatars/avatar-2.jpg" alt=""
                                        class="absolute h-full rounded-full w-full">
                                    <span
                                        class="absolute bg-green-500 border-2 border-white bottom-0 h-3 m-0.5 right-0 rounded-full shadow-md w-3"></span>
                                </div>
                                <div class="flex-1 min-w-0 relative text-gray-500">
                                    <h4 class="text-black font-semibold dark:text-white">David Peterson</h4>
                                    <span class="absolute right-0 top-1 text-xs">Sun</span>
                                    <p class="truncate">Esmod tincidunt ut laoreet</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="block flex items-center py-3 px-4 space-x-3 bg-gray-100 dark:bg-gray-700">
                                <div class="w-12 h-12 rounded-full relative flex-shrink-0">
                                    <img src="assets/images/avatars/avatar-1.jpg" alt=""
                                        class="absolute h-full rounded-full w-full">
                                    <span
                                        class="absolute bg-green-500 border-2 border-white bottom-0 h-3 m-0.5 right-0 rounded-full shadow-md w-3"></span>
                                </div>
                                <div class="flex-1 min-w-0 relative text-gray-500">
                                    <h4 class="text-black font-semibold dark:text-white">Sindy Forest</h4>
                                    <span class="absolute right-0 top-1 text-xs"> Mon</span>
                                    <p class="truncate">Seddiam nonummy nibh euismod laoreet</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#"
                                class="block flex items-center py-3 px-4 space-x-3 hover:bg-gray-100 dark:hover:bg-gray-700">
                                <div class="w-12 h-12 rounded-full relative flex-shrink-0">
                                    <img src="assets/images/avatars/avatar-5.jpg" alt=""
                                        class="absolute h-full rounded-full w-full">
                                    <span
                                        class="absolute bg-gray-300 border-2 border-white bottom-0 h-3 m-0.5 right-0 rounded-full shadow-md w-3"></span>
                                </div>
                                <div class="flex-1 min-w-0 relative text-gray-500">
                                    <h4 class="text-black font-semibold dark:text-white"> Zara Ali </h4>
                                    <span class="absolute right-0 top-1 text-xs">4 hours ago</span>
                                    <p class="truncate">Consectetuer adiscing elit</p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#"
                                class="block flex items-center py-3 px-4 space-x-3 hover:bg-gray-100 dark:hover:bg-gray-700">
                                <div class="w-12 h-12 rounded-full relative flex-shrink-0">
                                    <img src="assets/images/avatars/avatar-4.jpg" alt=""
                                        class="absolute h-full rounded-full w-full">
                                    <span
                                        class="absolute bg-green-500 border-2 border-white bottom-0 h-3 m-0.5 right-0 rounded-full shadow-md w-3"></span>
                                </div>
                                <div class="flex-1 min-w-0 relative text-gray-500">
                                    <h4 class="text-black font-semibold dark:text-white">David Peterson</h4>
                                    <span class="absolute right-0 top-1 text-xs">Week ago</span>
                                    <p class="truncate">Nam liber tempor <i class="uil-grin-tongue-wink"></i></p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#"
                                class="block flex items-center py-3 px-4 space-x-3 hover:bg-gray-100 dark:hover:bg-gray-700">
                                <div class="w-12 h-12 rounded-full relative flex-shrink-0">
                                    <img src="assets/images/avatars/avatar-2.jpg" alt=""
                                        class="absolute h-full rounded-full w-full">
                                    <span
                                        class="absolute bg-green-500 border-2 border-white bottom-0 h-3 m-0.5 right-0 rounded-full shadow-md w-3"></span>
                                </div>
                                <div class="flex-1 min-w-0 relative text-gray-500">
                                    <h4 class="text-black font-semibold dark:text-white">David Peterson</h4>
                                    <span class="absolute right-0 top-1 text-xs">Week ago</span>
                                    <p class="truncate">Esmod tincidunt ut laoreet</p>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!--  message-->

            @livewire('chat-form')


        </div>

    </div>



@endsection
