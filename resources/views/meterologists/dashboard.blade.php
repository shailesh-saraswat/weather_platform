@extends('layouts/meterologists')
@section('pagetitle')
Dashboard
@stop
@section('content')	
<div class="container m-auto">

	<h1 class="lg:text-2xl text-lg font-extrabold leading-none text-gray-900 tracking-tight mb-5"> Feed </h1>

	<div class="lg:flex justify-center lg:space-x-10 lg:space-y-0 space-y-5">

		<!-- Middle Section Start -->
		
		<div class="space-y-5 flex-shrink-0 lg:w-7/12">

			<!-- post 1-->
			<div class="bgMainColor bg-white shadow rounded-md dark:bg-gray-900 -mx-2 lg:mx-0">

				<!-- post header-->
				<div class="topPat flex justify-between items-center px-4 py-3">
					<div class="flex flex-1 items-center space-x-4">
						<a href="#">
							<div class="bg-gradient-to-tr from-yellow-600 to-pink-600 p-0.5 rounded-full">  
								<img src="{{url('assets/front_end/assets/images/avatars/avatar-2.jpg')}}" class="bg-gray-200 border border-white rounded-full w-8 h-8">
							</div>
						</a>
						<span class="block capitalize font-semibold dark:text-gray-100"> Johnson smith </span>
					</div>

				</div>
				
				<div class="midboxWrapper">
					<div class="topSectionWrap">
						<div class="topSection">
							<span> 28 </span>
							<img src="{{url('assets/front_end/assets/images/cloudsun.png')}}">
						</div>
						<div class="aboutWeatherWrap">
							<ul>
								<li> Sunny Day</li>
								<li> Feel Like</li>
							</ul>
						</div>									
					</div>
					<div class="weatherWSSPWrap">
						<ul>
							<li><img src="{{url('assets/front_end/assets/images/wind.svg')}}"><span> 13km/h </span></li>
							<li><img src="{{url('assets/front_end/assets/images/sunny-day.svg')}}"><span> UV-3 </span></li>
							<li><img src="{{url('assets/front_end/assets/images/precipitation.svg')}}"><span> 2% </span></li>
							<li><img src="{{url('assets/front_end/assets/images/snowflake.svg')}}"><span> 13km/h </span></li>
							<li><img src="{{url('assets/front_end/assets/images/sunny-day.svg')}}"><span> UV-3 </span></li>
							<li><img src="{{url('assets/front_end/assets/images/precipitation.svg')}}"><span> 2% </span></li>
						</ul>
					
					</div>
				</div>
				
				<div class="alretWrapper">
					<a href="sendalert.html">
						<div>
							<img src="{{url('assets/front_end/assets/images/walertbanner.png')}}">
						</div>
						<div class="mayorAlertWrap">
							<img class="mayorAlert" src="{{url('assets/front_end/assets/images/alert_icon.svg')}}">
							<h2> Send Weather Alert </h2>
							<p> Notification!!! </p>
						</div>
					</a>
				</div>
			</div>

		</div>
		
		<!-- Middle Section End -->

		<!-- Right Sidebar Start -->
		
		<div class="lg:w-5/12">

			<div class="bg-white shadow-md rounded-md overflow-hidden">

				<div class="bg-gray-50 dark:bg-gray-800 border-b border-gray-100 flex items-baseline justify-between py-4 px-6 dark:border-gray-800">
					<h2 class="font-semibold text-lg">User Voter List</h2>
					<a href="weathermetrologist.html"> See All</a>
				</div>
			   
				<div class="divide-gray-300 divide-gray-50 divide-opacity-50 divide-y px-4 dark:divide-gray-800 dark:text-gray-100">
					<div class="flex items-center justify-between py-3">
						<div class="flex flex-1 items-center space-x-4">
							<a href="profile.html">
								<img src="{{url('assets/front_end/assets/images/avatars/avatar-2.jpg')}}" class="bg-gray-200 rounded-full w-10 h-10">
							</a>
							<div class="flex flex-col">
								<a href="metrologistProfile.html">
									<span class="block capitalize font-semibold"> Johnson smith </span>
									<span class="block capitalize text-sm"> Spain(Barcelona) </span>
								</a>
							</div>
						</div>
						
						<a href="#" class="followButton"> Follow </a>
					</div>
					<div class="flex items-center justify-between py-3">
						<div class="flex flex-1 items-center space-x-4">
							<a href="profile.html">
								<img src="{{url('assets/front_end/assets/images/avatars/avatar-1.jpg')}}" class="bg-gray-200 rounded-full w-10 h-10">
							</a>
							<div class="flex flex-col">
								<a href="metrologistProfile.html">
									<span class="block capitalize font-semibold"> James Lewis </span>
									<span class="block capitalize text-sm"> Spain(Barcelona) </span>
								</a>
							</div>
						</div>
						<a href="#" class="followButton"> Follow </a>
					</div>
					<div class="flex items-center justify-between py-3">
						<div class="flex flex-1 items-center space-x-4">
							<a href="profile.html">
								<img src="{{url('assets/front_end/assets/images/avatars/avatar-5.jpg')}}" class="bg-gray-200 rounded-full w-10 h-10">
							</a>
							<div class="flex flex-col">
								<a href="metrologistProfile.html">
									<span class="block capitalize font-semibold"> John Michael </span>
									<span class="block capitalize text-sm"> Spain(Barcelona) </span>
								</a>
							</div>
						</div>
						<a href="#" class="followButton"> Follow </a>
					</div>
					<div class="flex items-center justify-between py-3">
						<div class="flex flex-1 items-center space-x-4">
							<a href="profile.html">
								<img src="{{url('assets/front_end/assets/images/avatars/avatar-7.jpg')}}" class="bg-gray-200 rounded-full w-10 h-10">
							</a>
							<div class="flex flex-col">
								<a href="metrologistProfile.html">
									<span class="block capitalize font-semibold"> Monroe Parker </span>
									<span class="block capitalize text-sm"> Spain(Barcelona) </span>
								</a>
							</div>
						</div>
						
						<a href="#" class="followButton"> Follow </a>
					</div>
					
					<div class="flex items-center justify-between py-3">
						<div class="flex flex-1 items-center space-x-4">
							<a href="profile.html">
								<img src="{{url('assets/front_end/assets/images/avatars/avatar-3.jpg')}}" class="bg-gray-200 rounded-full w-10 h-10">
							</a>
							<div class="flex flex-col">
								<a href="metrologistProfile.html">
									<span class="block capitalize font-semibold"> David Warner </span>
									<span class="block capitalize text-sm"> Spain(Barcelona) </span>
								</a>
							</div>
						</div>
						
						<a href="#" class="followButton"> Follow </a>
					</div>								

				</div>

			</div>

			<div class="mt-5">
				<div class="bg-white overflow-hidden">
					<div class="bg-gray-50 border-b py-4 px-6">
						<h2 class="font-semibold text-lg">Mayor Of the Barcelona</h2>
					</div>
					<div class="mayorWrap">
						<span class="mayorImgCrownWrap">
							<a href="mayorProfile.html">
								<img src="{{url('assets/front_end/assets/images/avatars/avatar-8.jpg')}}" class="mayorImage bg-gray-200 rounded-full w-10 h-10">
							</a>
							<img class="mayorCrown" src="{{url('assets/front_end/assets/images/crown.svg')}}">
						</span>
					</div>
					<div class="mayorNamecityWrap">
						<span class="mayorName"> Alfredo </span> is the Mayor of the <span class="mayorCity"> Barcelona City </span>
					</div>
				</div>
			</div>
		</div>

		<!-- Right Sidebar End -->
		
	</div>

</div>    
@endsection           
		  

		