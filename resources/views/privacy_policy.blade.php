@extends('layouts/front')
@section('pagetitle')
Privacy Policy
@stop
@section('content')
<div class="container inside-top-mar">
	<div class="innerwrap otxtWrap">
	
	  <h3>Privacy Policy</h3>

	  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum egestas elit et convallis. Ut vulputate ipsum eget nulla porta, non tristique sapien mollis. Curabitur lobortis <a href="#">www.weather.com</a> placerat metus, id gravida lorem lacinia nec. Vivamus lacinia dui id metus varius, consequat lobortis magna convallis. In nisi tortor, tincidunt malesuada tortor vitae, varius vulputate felis.</p>

	  <h4>Personal identification information</h4>

	  <p>Maecenas a ligula quis tortor commodo auctor. Integer vitae pulvinar mi, nec cursus ipsum. Vivamus vel fringilla velit, ac molestie nisl. Phasellus ultricies tincidunt gravida. Praesent molestie purus vel mattis tristique. Nam in quam est. Phasellus quis massa in velit tristique laoreet. Nam non elementum purus. Cras lacinia sodales diam. Nam in turpis tortor. Pellentesque fringilla dolor pharetra lorem accumsan venenatis et interdum velit. Aliquam erat volutpat.</p>

	  <h4>Non-personal identification information</h4>

	  <p>Ut non auctor libero. Etiam non dui sed quam tempor ullamcorper a nec ante. Maecenas rutrum turpis non feugiat dignissim. Donec mollis diam pellentesque consectetur aliquam. Proin suscipit et massa non interdum. Nam scelerisque lorem non nunc mattis, sed pretium nisl euismod. Proin semper risus eu odio scelerisque interdum. Duis quis metus vel mauris mollis imperdiet. Duis auctor malesuada maximus. Quisque elementum faucibus nulla, consectetur placerat erat finibus nec. Proin vitae luctus eros. Curabitur eu eros sit amet metus dictum tempus ac at felis. Quisque venenatis semper ullamcorper.</p>

	  <h4>Web browser cookies</h4>

	  <p>Sed ut ante varius, dapibus arcu a, consequat libero. Ut sem velit, tincidunt non nibh quis, tempus rhoncus tellus. Pellentesque eu turpis et elit suscipit cursus. Proin tincidunt eleifend neque, nec porta orci aliquam non. Etiam egestas eleifend porttitor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam a metus eu urna iaculis blandit sed et orci. </p>

	  <h4>How we use collected information</h4>

	  <p>Pellentesque sagittis nisl sed sem accumsan suscipit. Nam cursus consequat nunc. Cras eget eros justo. In at consequat dolor. Suspendisse potenti. Maecenas sodales consequat tellus non condimentum. Quisque in euismod arcu. Ut a commodo lacus. Donec elementum mi ut ligula suscipit, eu fringilla leo rhoncus. </p>

	  <ul>
		<li><em>To personalize user experience</em><br>
		Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer hendrerit risus a ligula rutrum.</li>
		<li><em>To improve our Site</em><br>
		Morbi faucibus justo leo, vel maximus odio mattis id. Nulla ultricies venenatis mauris vel molestie. Aenean commodo lectus fermentum porttitor.</li>
		<li><em>To process news</em><br>
		Nulla fringilla, nulla et mollis viverra, justo sapien elementum neque, sed molestie lacus enim nec erat. Vestibulum rhoncus, augue id laoreet mattis, odio dolor congue justo, nec convallis lorem elit vel est</li>
		<li><em>To send periodic emails</em><br>
		Mauris pulvinar sit amet erat sed pellentesque. Integer nec ipsum id risus suscipit ullamcorper at sit amet mi. Etiam justo sem, ultricies et pretium vitae, rutrum eu ligula. Maecenas vulputate nisi lectus, ac viverra nunc dapibus nec. Phasellus auctor lorem in mi convallis tempor. </li>
	  </ul>

	  <h4>How we protect your information</h4>
	  <p>Morbi semper finibus nisl, congue accumsan ante facilisis vel. Quisque eget elit felis. Nunc non efficitur lectus. Duis metus tortor, blandit tincidunt maximus eu, tristique ut augue. Duis eget purus ut velit finibus hendrerit. Proin congue purus vel fermentum pellentesque. In eget egestas leo. Maecenas in aliquam massa.</p>

	  <h4>Sharing your personal information</h4>

	  <p>Donec vel erat dapibus enim mattis ultricies vel in dolor. Ut gravida ligula quis magna lobortis, gravida condimentum eros dapibus. Duis feugiat sapien in nunc pulvinar suscipit. Phasellus sit amet justo vehicula, euismod tellus quis, porta eros. Aliquam leo metus, iaculis at ipsum in, suscipit aliquam mauris. Nunc ac dui pellentesque, posuere turpis id, suscipit quam. Donec porttitor diam id purus ornare suscipit.Quisque ac porta libero, sit amet malesuada tellus. Praesent semper sed tellus ac maximus. Quisque semper dui risus, in pharetra massa placerat id. Donec viverra pharetra viverra. Donec molestie nisl ultrices facilisis pharetra. Proin bibendum et erat vel laoreet. Suspendisse quis finibus.</p>

	  <h4>Third party websites</h4>

	  <p>Integer placerat consequat massa, sit amet ultrices sem scelerisque elementum. Sed eget pulvinar risus. Sed sollicitudin diam ac odio convallis, sit amet condimentum libero tempus. Nunc lobortis molestie ex eget consequat. Sed egestas lectus sit amet vehicula commodo. Praesent tristique velit iaculis arcu facilisis, vitae blandit lacus feugiat. Integer rutrum ornare mi, sed mattis dolor congue ut. Phasellus aliquet nibh quis pretium faucibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Proin eros eros, interdum sit amet lorem in, blandit ullamcorper nisi. Phasellus enim mauris, consectetur ut nulla finibus, auctor dignissim mauris. Donec imperdiet tristique felis, id aliquet ligula interdum vitae. </p>

	  <h4>Advertising</h4>

	  <p>Vivamus ullamcorper ac tortor vitae posuere. Ut sed odio risus. Suspendisse dictum ultricies fringilla. Nunc mollis ullamcorper ex, ut semper ex iaculis et. Nulla augue lectus, vulputate sit amet ligula in, commodo iaculis magna. Duis sed diam nec nulla dignissim cursus. Suspendisse id enim eget dolor posuere ultricies. Phasellus vitae maximus lectus, quis rutrum erat. Donec dapibus ullamcorper sem, sit amet lacinia nulla tincidunt eget. Praesent ut eleifend neque. Sed tincidunt, eros eget pellentesque tincidunt, tellus urna cursus arcu, non elementum libero leo vitae tortor. Nulla facilisi. Nullam vel orci ornare, mollis elit nec, viverra est.</p>

	  <h4>Compliance with children's online privacy protection act</h4>

	  <p>Morbi at mollis elit. Nunc suscipit ullamcorper dolor sed pretium. Etiam scelerisque placerat vestibulum. Nulla facilisi. Vestibulum aliquet faucibus est. Fusce at ipsum luctus, feugiat quam nec, semper mi. Nunc rutrum vitae orci sit amet faucibus. Donec tellus metus, tempor vel enim eget, imperdiet tincidunt nibh. Duis ut tincidunt sapien. Nullam imperdiet id nisi eu aliquet. Donec id risus dapibus massa molestie tempor quis eu turpis. Donec pretium viverra tempor. Nam vehicula tincidunt nisl, a ultrices diam volutpat id. Vestibulum tincidunt molestie tortor, vitae interdum dolor. In hac habitasse platea dictumst. Nam vel facilisis ex, eu dapibus diam.</p>

	  <h4>Changes to this privacy policy</h4>

	  <p>Suspendisse potenti. Phasellus convallis odio a purus sodales, non facilisis velit ultricies. Integer efficitur pellentesque lectus vel dictum. Suspendisse convallis semper pellentesque. Praesent convallis accumsan dui, sed fringilla felis iaculis non. Maecenas mollis dolor ipsum, vel tristique eros blandit at. Donec fermentum est non nunc iaculis pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>

	  <h4>Your acceptance of these terms</h4>

	  <p>Ut dictum ornare leo sit amet eleifend. Praesent posuere aliquam ipsum, ut pulvinar nibh mollis feugiat. Aenean non mi posuere, eleifend nunc consequat, tincidunt purus. Aliquam erat volutpat. Ut sed interdum magna, eget suscipit nulla. Morbi efficitur tellus non ante suscipit accumsan. Pellentesque in eros accumsan, tincidunt nunc eget, vestibulum purus. Curabitur mattis sit amet nisi et molestie.</p>

	  <h4>Contacting us</h4>

	  <p>Etiam non vestibulum velit. Morbi sed nunc tincidunt, porta elit id, porttitor nulla. Fusce sagittis libero justo, non tincidunt lacus faucibus nec. Nam nec est vitae dolor venenatis fringilla. Quisque tincidunt lobortis metus ac iaculis.</p>
	  <p>
		Weather (lorem ipsum)<br>
		<a href="#">www.weather.com</a><br>
		400 Maecenas fringilla sodales sem eget eleifend., CA 11111<br>
		privacy@weather.com<br>
	  </p>

	  <p><i>This document was last updated on May 30, 2021</i></p>


	</div>
</div>
@endsection