@extends('layouts/front')
@section('pagetitle')
    Home
@stop
@section('content')
    <section class="banner-1">
        <div class="container">
            <div class="row">
                <div class="search-icon">
                    <div class="two-grid">
                        @livewire('homeweather');
                    </div>
                </div>
    </section>

    <div class="interactiveMapContainer container wow  fadeInUp p0 animated" data-wow-delay="0.2s"
        style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
        <div class="row">
            <div class="all-container col-12">
                <div class="eight-container">

                    <div class="one-grid-container">
                        <div class="post-content mb20">
                            <a href=" ">
                                <img src="{{ url('assets/front/images/map1.jpg') }}">
                                <div class="eight-big-container">
                                    <div class="eight-big-content">
                                        <h2>See Interactive Map</h2>
                                    </div>

                                </div>
                            </a>
                        </div>
                        <div class="newheading">
                            <h2>WEATHER NEWS</h2>
                        </div>

                        <div class="slider-container two-grid-container news1">

                            <img src="{{ url('assets/front/images/news1.png') }}">

                            <div class="slider-content">
                                <h2><span>Hurricane</span> ANA BECOMES FIRST NAMED SYSTEM OF 2021 IN ATLANTIC</h2>


                                <h3>MAY 24, 2021</h3>


                            </div>
                        </div>
                        <div class="slider-container two-grid-container news1">

                            <img src="{{ url('assets/front/images/news2.jpg') }}">

                            <div class="slider-content">
                                <h2><span>Hurricane</span> ANA BECOMES FIRST NAMED SYSTEM OF 2021 IN ATLANTIC</h2>


                                <h3>MAY 24, 2021</h3>


                            </div>
                        </div>
                        <div class="slider-container two-grid-container news1">

                            <img src="{{ url('assets/front/images/news3.jpg') }}">

                            <div class="slider-content">
                                <h2><span>Hurricane</span> ANA BECOMES FIRST NAMED SYSTEM OF 2021 IN ATLANTIC</h2>


                                <h3>MAY 24, 2021</h3>


                            </div>
                        </div>
                        <div class="slider-container two-grid-container news1">

                            <img src="{{ url('assets/front/images/news4.png') }}">

                            <div class="slider-content">
                                <h2><span>Hurricane</span> ANA BECOMES FIRST NAMED SYSTEM OF 2021 IN ATLANTIC</h2>


                                <h3>MAY 24, 2021</h3>


                            </div>
                        </div>

                        <div class="votes-now">
                            <div class="newheading">
                                <h2 class="mb20">VOTE NOW</h2>
                            </div>

                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="pill" href="#home">Today</a></li>
                                <li><a data-toggle="pill" href="#menu1">Tomorrow</a></li>
                                <li><a data-toggle="pill" href="#menu2">Day After Tomorrow</a></li>
                            </ul>
                            {{-- <form> --}}
                            <div class="tab-content">

                                <div id="home" class="tab-pane fade in active">
                                    <h3>Today Voted Temprature <a href="">See all</a></h3>
                                    <div class="user-voted">
                                        <h4>David<span>High Temp - 48 | 32 Votes</span></h4>
                                    </div>
                                    <div class="user-voted">
                                        <h4>David<span>High Temp - 48 | 32 Votes</span></h4>
                                    </div>
                                    <div class="user-voted">
                                        <h4>David<span>High Temp - 48 | 32 Votes</span></h4>
                                    </div>
                                    <div class="user-voted">
                                        <h4>David<span>High Temp - 48 | 32 Votes</span></h4>
                                    </div>
                                </div>
                                <div id="menu1" class="tab-pane fade">
                                    @livewire('weathervote')
                                    {{-- <h3>Select High or Low Temprature</h3>
                                    <div class="enter-temp">
                                        <div class="form-group">
                                            <label> Select list</label>
                                            <select>
                                                <option>Low Temprature</option>
                                                <option>High Temprature</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label> Enter temp. value</label>
                                            <input type="text" placeholder="Hint : 28">
                                        </div>
                                        <div class="form-group">
                                            <label> Select list</label>
                                            <select>
                                                <option>No precipitation</option>
                                                <option>Rain</option>
                                                <option>Snow</option>
                                                <option>Ice</option>
                                                <option>Mixed</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="enter-temp-bt">Submit</button>

                                    </div> --}}

                                </div>

                                <div id="menu2" class="tab-pane fade">
                                    @livewire('weathervotenextday')
                                    {{-- <h3>Select High or Low Temprature</h3>
                                        <div class="enter-temp">
                                            <div class="form-group">
                                                <label> Select list</label>
                                                <select>
                                                    <option>Low Temprature</option>
                                                    <option>High Temprature</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label> Enter temp. value</label>
                                                <input type="text" placeholder="Hint : 28">
                                            </div>
                                            <div class="form-group">
                                                <label> Select list</label>
                                                <select>
                                                    <option>No precipitation</option>
                                                    <option>Rain</option>
                                                    <option>Snow</option>
                                                    <option>Ice</option>
                                                    <option>Mixed</option>
                                                </select>
                                            </div>
                                        </div>
                                        <button type="submit" class="enter-temp-bt">Submit</button> --}}
                                </div>
                                {{-- </form> --}}
                            </div>
                        </div>















                    </div>
                </div>



















                <div class="three-container">

                    <div class="one-grid-container latest">
                        <div class="image-sidepost">
                            <img src="{{ url('assets/front/images/blog1.png') }}">
                            <h2>Cicada palooza' is underway, but it won't last long</h2>
                            <p>The Brood X cicadas' emergence is tied to a specific ground temperature, but what sends them
                                back
                                underground for another 17 years? And when will it happen?</p>
                        </div>
                        <div class="image-sidepost">
                            <img src="{{ url('assets/front/images/blog2.jpg') }}">
                            <h2>Cicada palooza' is underway, but it won't last long</h2>
                            <p>The Brood X cicadas' emergence is tied to a specific ground temperature, but what sends them
                                back
                                underground for another 17 years? And when will it happen?</p>
                        </div>
                        <div class="image-sidepost">
                            <img src="{{ url('assets/front/images/blog3.jpg') }}">
                            <h2>Cicada palooza' is underway, but it won't last long</h2>
                            <p>The Brood X cicadas' emergence is tied to a specific ground temperature, but what sends them
                                back
                                underground for another 17 years? And when will it happen?</p>
                        </div>
                        <a href="" class="see-all">See all </a>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="download-area pt-50  ">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-4 col-sm-12 d-sm-none d-md-block">
                    <div class="download_image">
                        <img class="dw  " src="{{ url('assets/front/images/iphon2.png') }}"
                            alt="download_shape">
                        <img class="downloadpos  " src="{{ url('assets/front/images/iphone3.png') }}"
                            alt="download_shape">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12">
                    <div class="feature-box-layout2 d-f">
                        <div class=" ">
                            <div class="translate-bottom-75 opacity-animation transition-150 transition-delay-200">
                                <h3 class="item-title">Download Your Amazing App Available Now.</h3>
                            </div>
                            <div class="translate-bottom-75 opacity-animation transition-150 transition-delay-500">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting has been the industry's
                                    standard dummy text ever since </p>
                            </div>
                            <div class="play-store">
                                <img src="{{ url('assets/front/images/apple.png') }}">
                                <img src="{{ url('assets/front/images/android.png') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="clear:both;"></div>
    @livewireScripts()
    <script type="text/javascript">
        if (navigator.geolocation) {
            var APP_URL = {!! json_encode(url('/')) !!}
            navigator.geolocation.getCurrentPosition(function(position) {
                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;
                livewire.emit('getLatitudeForInput', latitude,
                    longitude); // call livewire component with the get data of latitude and longitute
            });
        }
    </script>
@endsection
