<div class="temp">
    <h2>
        @if (!empty($weather_result))

            {{-- @else --}}
            {{ $weather_result[0]->Temperature->Metric->Value }}
        @endif
        {{-- <input wire:model="latitude" id="lat-span" /> --}}

        <sup>0</sup>
    </h2>
</div>
<div class="temp-icon">
    <img src="{{ url('assets/front/images/Partially_cloud.svg') }}">
</div>
</div>
<form>
    <input type="search" placeholder="Search Location"><i class="fas fa-search"></i>
</form>
<div class="banner-con">
    <div class="ww-details">

        <div class="three-grid">
            @if (!empty($weather_result))
                {{-- @else --}}
                <div class="three-column">

                    <h3>{{ $weather_result[0]->Wind->Speed->Metric->Value }}km/h </h3>

                    <img src="{{ url('assets/front/images/snowflake.svg') }}">
                </div>
            @endif
            @if (!empty($weather_result))
                {{-- @else --}}
                <div class="three-column">

                    <h3>UV-{{ $weather_result[0]->UVIndex }} </h3>

                    <img src="{{ url('assets/front/images/sunny_day.svg') }}">
                </div>
            @endif
            @if (!empty($weather_result))
                {{-- @else --}}
                <div class="three-column">

                    <h3> {{ $weather_result[0]->Precip1hr->Metric->Value }}mm</h3>

                    <img src="{{ url('assets/front/images/wind.svg') }}">
                </div>
            @endif
            {{-- <div class="three-column">
                <h3>10 </h3>
                <img src="{{ url('assets/front/images/precipitation.svg') }}">
            </div>
            <div class="three-column">
                <h3>10 </h3>
                <img src="{{ url('assets/front/images/snowflake.svg') }}">
            </div>
            <div class="three-column">
                <h3>10 </h3>
                <img src="{{ url('assets/front/images/snowflake.svg') }}">
            </div> --}}
        </div>
    </div>
</div>
</div>
