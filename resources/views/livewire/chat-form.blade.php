{{-- <div> --}}
{{-- If you look to others for fulfillment, you will never truly be fulfilled. --}}

<div class="lg:w-8/12 bg-white dark:bg-gray-800">

    <div class="px-5 py-4 flex uk-flex-between">

        <a href="#" class="flex items-center space-x-3">
            <div class="w-10 h-10 rounded-full relative flex-shrink-0">
                <img src="assets/images/avatars/avatar-1.jpg" alt="" class="h-full rounded-full w-full">
                <span
                    class="absolute bg-green-500 border-2 border-white bottom-0 h-3 m-0.5 right-0 rounded-full shadow-md w-3"></span>
            </div>
            <div class="flex-1 min-w-0 relative text-gray-500">
                <h4 class="font-semibold text-black text-lg">Sindy Forest</h4>

                <p class="font-semibold leading-3 text-green-500 text-sm">is online</p>
            </div>
        </a>

        <a href="#" class="flex hover:text-red-400 items-center leading-8 space-x-2 text-red-500 font-medium">
            <i class="uil-trash-alt"></i> <span class="lg:block hidden"> Delete Conversation </span>
        </a>
    </div>

    <div class="border-t dark:border-gray-600">

        <!--Chat List show-->
        @livewire('chat-list');

        <div class="border-t flex p-6 dark:border-gray-700">
            <textarea wire:model="message" wire:keydown.enter="sendmessage" cols="1" rows="1"
                placeholder="Your Message.."
                class="border-0 flex-1 h-10 min-h-0 resize-none min-w-0 shadow-none dark:bg-transparent"></textarea>
            <div class="flex h-full space-x-2">
                <button type="submit" class="bg-blue-600 font-semibold px-6 py-2 rounded-md text-white"
                    wire:click="sendmessage" wire:loading.attr="disabled" wire:offline.attr="disabled">Send</button>
            </div>
        </div>
        <!--message display-->
        <div class="row">
            <div class="col-6">
                <!-- messages de alerta -->
                <div style="position: absolute;" class="alert alert-success collapse" role="alert" id="successmsg">Se
                    ha
                    enviado</div>
            </div>
            {{-- <div class="col-6 pt-2 text-right">
                <button class="btn btn-primary" wire:click="enviarmessage" wire:loading.attr="disabled"
                    wire:offline.attr="disabled">Enviar message</button>
            </div> --}}
        </div>

    </div>

</div>
<script>
    // Esto lo recibimos en JS cuando lo emite el componente
    // El evento "enviadoOK"
    $(document).ready(function() {
        window.livewire.on('successmsg', function() {
            $("#successmsg").fadeIn("slow");
            setTimeout(function() {
                $("#successmsg").fadeOut("slow");
            }, 3000);
        });
    });
</script>
{{-- </div> --}}
