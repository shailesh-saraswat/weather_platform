{{-- <div>
</div> --}}
{{-- <div class="lg:p-8 p-4 space-y-5">

    <h3 class="lg:w-60 mx-auto text-sm uk-heading-line uk-text-center lg:pt-2"><span> 28 June, 2018
        </span></h3>

    <!-- my message-->
    <div class="flex lg:items-center flex-row-reverse">
        <div class="w-14 h-14 rounded-full relative flex-shrink-0">
            <img src="assets/images/avatars/avatar-2.jpg" alt="" class="absolute h-full rounded-full w-full">
        </div>
        <div class="text-white py-2 px-3 rounded bg-blue-600 relative h-full lg:mr-5 mr-2 lg:ml-20">
            <p class="leading-6">consectetuer adipiscing elit, sed diam nonummy nibh euismod
                laoreet dolore magna <i class="uil-grin-tongue-wink"></i> </p>
            <div class="absolute w-3 h-3 top-3 -right-1 bg-blue-600 transform rotate-45"></div>
        </div>
    </div>

    <h3 class="lg:w-60 mx-auto text-sm uk-heading-line uk-text-center lg:pt-2"><span> 28 June, 2018
        </span></h3>

    <div class="flex lg:items-center">
        <div class="w-14 h-14 rounded-full relative flex-shrink-0">
            <img src="assets/images/avatars/avatar-1.jpg" alt="" class="absolute h-full rounded-full w-full">
        </div>
        <div
            class="text-gray-700 py-2 px-3 rounded bg-gray-100 h-full relative lg:ml-5 ml-2 lg:mr-20 dark:bg-gray-700 dark:text-white">
            <p class="leading-6">In ut odio libero vulputate <urna class="i uil-heart"></urna>
                <i class="uil-grin-tongue-wink"> </i>
            </p>
            <div class="absolute w-3 h-3 top-3 -left-1 bg-gray-100 transform rotate-45 dark:bg-gray-700">
            </div>
        </div>
    </div>

    <!-- my message-->
    <div class="flex lg:items-center flex-row-reverse">
        <div class="w-14 h-14 rounded-full relative flex-shrink-0">
            <img src="assets/images/avatars/avatar-2.jpg" alt="" class="absolute h-full rounded-full w-full">
        </div>
        <div class="text-white py-2 px-3 rounded bg-blue-600 relative h-full lg:mr-5 mr-2 lg:ml-20">
            <p class="leading-6">Nam liber tempor cum soluta nobis eleifend option <i
                    class="uil-grin-tongue-wink-alt"></i></p>
            <div class="absolute w-3 h-3 top-3 -right-1 bg-blue-600 transform rotate-45"></div>
        </div>
    </div>

    <h3 class="lg:w-60 mx-auto text-sm uk-heading-line uk-text-center lg:pt-2"><span> 28 June, 2018
        </span></h3>
    <div class="flex lg:items-center flex-row-reverse">
        <div class="w-14 h-14 rounded-full relative flex-shrink-0">
            <img src="assets/images/avatars/avatar-2.jpg" alt="" class="absolute h-full rounded-full w-full">
        </div>
        <div class="text-white py-2 px-3 rounded bg-blue-600 relative h-full lg:mr-5 mr-2 lg:ml-20">
            <p class="leading-6">consectetuer adipiscing elit, sed diam nonummy nibh euismod
                laoreet dolore magna.</p>
            <div class="absolute w-3 h-3 top-3 -right-1 bg-blue-600 transform rotate-45"></div>
        </div>
    </div>

    <h3 class="lg:w-60 mx-auto text-sm uk-heading-line uk-text-center lg:pt-2"><span> 28 June, 2018
        </span></h3>

    <div class="flex lg:items-center">
        <div class="w-14 h-14 rounded-full relative flex-shrink-0">
            <img src="assets/images/avatars/avatar-1.jpg" alt="" class="absolute h-full rounded-full w-full">
        </div>
        <div
            class="text-gray-700 py-2 px-3 rounded bg-gray-100 relative h-full lg:ml-5 ml-2 lg:mr-20 dark:bg-gray-700 dark:text-white">
            <p class="leading-6">Nam liber tempor cum soluta nobis eleifend option congue nihil
                imperdiet doming </p>
            <div class="absolute w-3 h-3 top-3 -left-1 bg-gray-100 transform rotate-45 dark:bg-gray-700">
            </div>
        </div>
    </div>

    <!-- my message-->

    <div class="flex lg:items-center flex-row-reverse">
        <div class="w-14 h-14 rounded-full relative flex-shrink-0">
            <img src="assets/images/avatars/avatar-2.jpg" alt="" class="absolute h-full rounded-full w-full">
        </div>
        <div class="text-white py-2 px-3 rounded bg-blue-600 relative h-full lg:mr-5 mr-2 lg:ml-20">
            <p class="leading-6">quis nostrud exerci tation ullamcorper suscipit .</p>
            <div class="absolute w-3 h-3 top-3 -right-1 bg-blue-600 transform rotate-45"></div>
        </div>
    </div>

    <div class="flex lg:items-center">
        <div class="w-14 h-14 rounded-full relative flex-shrink-0">
            <img src="assets/images/avatars/avatar-1.jpg" alt="" class="absolute h-full rounded-full w-full">
        </div>
        <div
            class="text-gray-700 py-2 px-3 rounded bg-gray-100 relative h-full lg:ml-5 ml-2 lg:mr-20 dark:bg-gray-700 dark:text-white">

            <div class="flex space-x-0.5 my-2 animate-pulse">
                <div class="w-2 h-2 rounded-full bg-gray-400"></div>
                <div class="w-2 h-2 rounded-full bg-gray-300"></div>
                <div class="w-2 h-2 rounded-full bg-gray-300"></div>
            </div>
            <div class="absolute w-3 h-3 top-3 -left-1 bg-gray-100 transform rotate-45 dark:bg-gray-700">
            </div>
        </div>
    </div>

</div> --}}
<div class="mt-3">

    <h3><strong>Últimos 5 messages</strong></h3>

    <div class="card">
        <div class="card-body">
            @foreach ($messages as $message)
                <div>

                    @if ($message['recibido'])
                        <div class="alert alert-warning" style="margin-right: 50px;">
                            <strong>{{ $message['username'] }}</strong><small
                                class="float-right">{{ $message['fecha'] }}</small>
                            <br><span class="text-muted">{{ $message['message'] }}</span>
                        </div>
                    @else
                        <div class="alert alert-success" style="margin-left: 50px;">
                            <strong>{{ $message['username'] }}</strong><small
                                class="float-right">{{ $message['fecha'] }}</small>
                            <br><span class="text-muted">{{ $message['message'] }}</span>
                        </div>
                    @endif

                </div>
            @endforeach
        </div>
    </div>


</div>

<script>
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}', {
        cluster: '{{ env('PUSHER_APP_CLUSTER') }}',
        forceTLS: true
    });

    // var channel = pusher.subscribe('private-App.User' + {{ Auth::user()->id }});
    var channel = pusher.subscribe('chat-channel');


    channel.bind('chat-event', function(data) {
        window.livewire.emit('messagereceived', data);
    });

    setTimeout(function() {
        window.livewire.emit('requestusername');
    }, 100);
</script>
{{-- <script>
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}', {
        cluster: '{{ env('PUSHER_APP_CLUSTER') }}',
        forceTLS: true
    });

    var channel = pusher.subscribe('private-chat-channel');

    channel.bind('chat-event', function(data) {
        window.livewire.emit('messagereceived', data);
    });

    setTimeout(function() {
        window.livewire.emit('requestusername');
    }, 100);
</script> --}}
