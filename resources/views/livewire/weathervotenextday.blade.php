<div class="radioButtonHighLowWrap">
    {{-- <div> --}}
    @if (session()->has('message'))
        <p class="alert alert-success" id="alert_box">{{ session('message') }}</p>
        {{-- <div class="alert alert-warning alert-dismissible" role="alert">
            <strong>Voted</strong> {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div> --}}
    @endif
    {{-- @if (session()->has('message'))

            <div class="p-3 bg-green-300 textgreen">

                {{ session('message') }}

            </div>

        @endif --}}

    {{-- </div> --}}
    <form method="POST" wire:submit.prevent="submit">
        @csrf
        <div class="radioButtonInline">
            <div class="imagePdiv">
                <label>
                    <input type="radio" wire:model="is_temp" value="0" checked="">
                    <div class="delOpt"> Low Temprature</div>
                    @error('is_temp')
                        <div class="alert alert-danger alert-dismissible custom_alert" style="display: block;">
                            <span class="alert_message">{{ $message }}</span>
                        </div>
                    @enderror
                </label>
            </div>

            <div class="imagePdiv">
                <label>
                    <input type="radio" wire:model="is_temp" value="1">
                    <div class="delOpt"> High Temprature </div>
                </label>
            </div>
        </div>
        <div class="addVoteWrapper">
            <div class="addVoteInput">
                <div class="avisHead"> Enter temp. value </div>
                <input type="number" wire:model="temp_value" placeholder="Hint :28">
                @error('temp_value')
                    <div class="alert alert-danger alert-dismissible custom_alert" style="display: block;">
                        <span class="alert_message">{{ $message }}</span>
                    </div>
                @enderror
            </div>
            <div class="addVoteSelect">
                <div class="avisHead"> Select list </div>
                @error('precipitation_id')
                    <div class="alert alert-danger alert-dismissible custom_alert" style="display: block;">
                        <span class="alert_message">{{ $message }}</span>
                    </div>
                @enderror
                <select wire:model="precipitation_id">
                    @foreach ($precipitation as $val)
                        <option value="{{ $val->id }}">
                            {{ $val->precipitation_name }}
                        </option>
                    @endforeach
                    {{-- @foreach ($precipitation as $val)
                        <option value="{{ $val->id }}">
                            {{ $val->precipitation_name }}
                        </option>
                    @endforeach --}}

                    {{-- <option value="1">Rain</option>
                    <option value="2">Snow</option>
                    <option value="3">Ice</option>
                    <option value="4">Mixed</option> --}}
                </select>
            </div>
        </div>
        <div class="voteSubmitDiv">
            <button class="voteSubmit"> Submit </button>
        </div>
    </form>
</div>
