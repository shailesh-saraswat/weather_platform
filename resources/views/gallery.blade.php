@extends('layouts/front')
@section('pagetitle')
Gallery
@stop
@section('content')
<section class="inside-top-mar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div>
					<h2>Gallery</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting has been the industry's standard dummy text ever sinceLorem Ipsum is simply dummy text of the printing and typesetting has been the industry's standard dummy text ever sinceLorem</p>				
				</div>
			</div>
		</div>
	</div>
</section>

<section id="portfolio" class="bg-light thrid-gray-right inside-top-margin">
   <div class="">
  

  
		<p><section class="img-gallery-magnific">
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/974/?random" title="9.jpg">
					<img src="https://unsplash.it/974/?random" alt="9.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/900/?random" title="10.jpg">
					<img src="https://unsplash.it/900/?random" alt="10.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/902" title="3.jpg">
					<img src="https://unsplash.it/902/" alt="3.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/901" title="4.jpg">
					<img src="https://unsplash.it/901" alt="4.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/888/?random" title="1.jpg">
					<img src="https://unsplash.it/888/?random" alt="1.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/931/?random" title="2.jpg">
					<img src="https://unsplash.it/931/?random" alt="2.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/908/?random" title="5.jpg">
					<img src="https://unsplash.it/908/?random" alt="5.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/978/?random" title="6.jpg">
					<img src="https://unsplash.it/978/?random" alt="6.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/857/?random" title="7.jpg">
					<img src="https://unsplash.it/857/?random" alt="7.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/905/?random" title="8.jpg">
					<img src="https://unsplash.it/905/?random" alt="8.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/1230/?random" title="12.jpg">
					<img src="https://unsplash.it/1230/?random" alt="12.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/800/?random" title="13.jpg">
					<img src="https://unsplash.it/800/?random" alt="13.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/930/?random" title="14.jpg">
					<img src="https://unsplash.it/930/?random" alt="14.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/999/?random" title="15.jpg">
					<img src="https://unsplash.it/999/?random" alt="15.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
			<div class="magnific-img">
				<a class="image-popup-vertical-fit" href="https://unsplash.it/897/?random" title="16.jpg">
					<img src="https://unsplash.it/897/?random" alt="16.jpg" />
					<i class="fa fa-search-plus" aria-hidden="true"></i>
				</a>
			</div>
		</section>
		<div class="clear"></div>
	</p>

</div>
</section>
@endsection