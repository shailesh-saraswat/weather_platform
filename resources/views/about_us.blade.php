@extends('layouts/front')
@section('pagetitle')
About Us
@stop
@section('content')
<div class="container inside-top-mar aboutSect1_container">
	<div class="row">
		<div class="two-grid-inside">
		<div class="right-abt">
				<h2>Welcome to Weather Forecast</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting has been the industry's standard dummy text ever sinceLorem Ipsum is simply dummy text of the printing and typesetting has been the industry's standard dummy text ever sinceLorem Ipsum is simply dummy text of the printing and typesetting has been the industry's standard dummy text ever sinceLorem Ipsum is simply dummy text of the printing and typesetting has been the industry's standard dummy text ever since</p>
				<ul>
					<li><img src="{{url('assets/front/images/but.svg')}}">Lorem Ipsum is simply dummy text of the printing  </li>
					<li><img src="{{url('assets/front/images/but.svg')}}">Industry's standard dummy text</li>
					<li><img src="{{url('assets/front/images/but.svg')}}">Simply dummy text of the printing and typesetting</li>
					<li><img src="{{url('assets/front/images/but.svg')}}">Text of the printing and typesetting</li>
				 
				</ul>
			</div>
			<div class="left-abt">
				<img src="{{url('assets/front/images/abt.svg')}}">
			</div>
			
		</div>
	</div>
</div>

<div style="clear:both;"></div>
<section class="app-section section-b-space aboutSect1_section"   style="width: 100%; " >
 <div >
 <video autoplay="" loop="" muted="" style="margin: auto; position: absolute; z-index: -1; top: 50%; left: 0%; transform: translate(0%, -50%); visibility: visible; opacity: 1; width: 1905px; height: auto;">
	<source src="{{url('assets/front/images/abt-vi2.mp4')}}" type="video/mp4">
	<source src="{{url('assets/front/images/abt-vi2.webm')}}" type="video/webm">
	<source src="{{url('assets/front/images/abt-vi2.ogv')}}" type="video/ogg">
</video>
</div>
        <div class="container">
            <div class="row order-cls">
                <div class="col-lg-12">
                    <div class="app-content">
                        <div animated="" animated-media-none="1" data-animation-type="fadeInUp" data-animation-duration="1">
                            <h2 class="title ">Sunshine is delicious, rain is refreshing, wind braces us up, snow is exhilarating; there is really no such thing as bad weather, only different kinds of good weather.</h2>
                            <a href="">Contact Us</a>
                           
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </section>
<section class=" pb60 pt80 bg-light aboutSect2_container">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="heading heading--two mb60 wow  fadeInUp animated" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                    
                    <h3 class="heading__title">
                        We`re Working Together
                    </h3>
                </div>
            </div>
        </div>
        <div class="row justify-content-center no-gutters grid-brand">
            <div class="col-12 col-lg-3 col-md-6 wow  fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                <div class="brand__item">
                    <img src="{{url('assets/front/images/1.png')}}" alt="">
                </div>
            </div>
            <div class="col-12 col-lg-3 col-md-6 wow  fadeInUp animated" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                <div class="brand__item">
                    <img src="{{url('assets/front/images/2.png')}}" alt="">
                </div>
            </div>
            <div class="col-12 col-lg-3 col-md-6 wow  fadeInUp animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="brand__item">
                    <img src="{{url('assets/front/images/3.png')}}" alt="">
                </div>
            </div>
            <div class="col-12 col-lg-3 col-md-6 wow  fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                <div class="brand__item">
                    <img src="{{url('assets/front/images/1.png')}}" alt="">
                </div>
            </div>
        </div>
        <div class="row justify-content-center no-gutters grid-brand">
            <div class="col-12 col-lg-3 col-md-6 wow  fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                <div class="brand__item">
                    <img src="{{url('assets/front/images/2.png')}}" alt="">
                </div>
            </div>
            <div class="col-12 col-lg-3 col-md-6 wow  fadeInUp animated" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                <div class="brand__item">
                    <img src="{{url('assets/front/images/3.png')}}" alt="">
                </div>
            </div>
            <div class="col-12 col-lg-3 col-md-6 wow  fadeInUp animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="brand__item">
                    <img src="{{url('assets/front/images/1.png')}}" alt="">
                </div>
            </div>
            <div class="col-12 col-lg-3 col-md-6 wow  fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                <div class="brand__item">
                    <img src="{{url('assets/front/images/2.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

 <div class="download-area pt-50  ">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-4 col-sm-12 d-sm-none d-md-block">
                    <div class="download_image">
                        <img class="dw  " src="{{url('assets/front/images/iphon2.png')}}" alt="download_shape">
                        <img class="downloadpos  " src="{{url('assets/front/images/iphone3.png')}}" alt="download_shape"> 
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12">
                   <div class="feature-box-layout2 d-f">
                   <div class=" ">
								<div class="translate-bottom-75 opacity-animation transition-150 transition-delay-200">
									<h3 class="item-title">Download Your Amazing App Available Now.</h3>
								</div>
								<div class="translate-bottom-75 opacity-animation transition-150 transition-delay-500">
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting has been the industry's standard dummy text ever since </p>
								</div> 
								<div class="play-store">
									<img src="{{url('assets/front/images/apple.png')}}">
									<img src="{{url('assets/front/images/android.png')}}">
								</div>
							</div>
							</div>
                </div>
            </div>
        </div>
    </div>
 <div style="clear:both;"></div>
@endsection