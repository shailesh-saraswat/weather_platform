@extends('layouts/front')
@section('pagetitle')
Contact Us
@stop
@section('content')
<div class="container inside-top-mar">
	<div class="innerwrap">
	
		<section class="section1 clearfix">
			<div class="textcenter">
				 
				<h2>Drop Us a Mail</h2>
			</div>
		</section>
	
		<section class="section2 clearfix">
			<div class="col2 column1 first">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2773037.0516941547!2d-123.12484735946003!3d47.254842973530195!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5485e5ffe7c3b0f9%3A0x944278686c5ff3ba!2sWashington%2C%20USA!5e0!3m2!1sen!2sin!4v1622131495660!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
			</div>
			<div class="col2 column2 last"> 
				<div class="sec2contactform">
					<h3 class="sec2frmtitle">Want to Know More?? Drop Us a Mail</h3>
					<form action="">
						<div class="clearfix">
							<input class="col2 first" type="text" placeholder="FirstName">
							<input class="col2 last" type="text" placeholder="LastName">
						</div>
						<div class="clearfix">
							<input  class="col2 first" type="Email" placeholder="Email">
							<input class="col2 last" type="text" placeholder="Contact Number">
						</div>
						<div class="clearfix">
							<textarea name="textarea" id="" cols="30" rows="7">Your message here...</textarea>
						</div>
						<div class="clearfix"><input type="submit" value="Send"></div>
					</form>
				</div>

			</div>
		</section>
	
	</div>
</div>
@endsection