<div class="topSectionWrap">
    <div class="topSection">
        <span>
            @if (!empty($weather_result))
                {{ $weather_result[0]->Temperature->Metric->Value }}
            @endif
        </span>
        <img src="{{ url('assets/front_end/assets/images/cloudsun.png') }}">
    </div>
    <div class="aboutWeatherWrap">
        <ul>
            @if (!empty($weather_result))
                <li> {{ $weather_result[0]->WeatherText }}</li>
                <li> Feel Like {{ $weather_result[0]->RealFeelTemperature->Metric->Value }}</li>
            @endif
        </ul>
    </div>
</div>
<div class="weatherWSSPWrap">
    <ul>
        @if (!empty($weather_result))
            <li><img src="{{ url('assets/front_end/assets/images/wind.svg') }}"><span>
                    {{ $weather_result[0]->Wind->Speed->Metric->Value }}km/h </span>
            </li>
            <li><img src="{{ url('assets/front_end/assets/images/sunny-day.svg') }}"><span>
                    UV-{{ $weather_result[0]->UVIndex }}
                </span>
            </li>

            <li><img src="{{ url('assets/front_end/assets/images/precipitation.svg') }}"><span>
                    {{ $weather_result[0]->Precip1hr->Metric->Value }}mm
                </span></li>
        @endif

        {{-- <li><img src="{{ url('assets/front_end/assets/images/snowflake.svg') }}"><span> 13km/h
            </span></li>
        <li><img src="{{ url('assets/front_end/assets/images/sunny-day.svg') }}"><span> UV-3
            </span>
        </li>
        <li><img src="{{ url('assets/front_end/assets/images/precipitation.svg') }}"><span> 2%
            </span></li> --}}
    </ul>

</div>
