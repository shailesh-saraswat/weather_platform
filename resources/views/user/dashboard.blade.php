@extends('layouts/user_front')
@section('pagetitle')
    Dashboard
@stop
@section('content')
    <div class="container m-auto">

        <h1 class="lg:text-2xl text-lg font-extrabold leading-none text-gray-900 tracking-tight mb-5"> Feed </h1>

        <div class="lg:flex justify-center lg:space-x-10 lg:space-y-0 space-y-5">

            <!-- Middle Section Start -->

            <div class="space-y-5 flex-shrink-0 lg:w-7/12">

                <!-- post 1-->
                <div class="bgMainColor bg-white shadow rounded-md dark:bg-gray-900 -mx-2 lg:mx-0">

                    <!-- post header-->
                    <div class="topPat flex justify-between items-center px-4 py-3">
                        <div class="flex flex-1 items-center space-x-4">
                            <a href="#">
                                <div class="bg-gradient-to-tr from-yellow-600 to-pink-600 p-0.5 rounded-full">
                                    <img src="{{ url('assets/front_end/assets/images/avatars/avatar-2.jpg') }}"
                                        class="bg-gray-200 border border-white rounded-full w-8 h-8">
                                </div>
                            </a>
                            <span class="block capitalize font-semibold dark:text-gray-100"> {{ $user->name }} </span>
                        </div>

                    </div>

                    <div class="midboxWrapper" id="weathertemp">
                        {{-- <div class="topSectionWrap">
                            <div class="topSection">
                                <span>
                                    @if (isset($weather_result[0]->Temperature->Metric->Value))
                                        {{ $weather_result[0]->Temperature->Metric->Value }}
                                    @endif
                                </span>
                                <img src="{{ url('assets/front_end/assets/images/cloudsun.png') }}">
                            </div>
                            <div class="aboutWeatherWrap">
                                <ul>
                                    <li> {{ $weather_result[0]->WeatherText }}</li>
                                    <li> Feel Like {{ $weather_result[0]->RealFeelTemperature->Metric->Value }}</li>
                                </ul>
                            </div>
                        </div> --}}
                        {{-- <div class="weatherWSSPWrap">
                            <ul>
                                <li><img src="{{ url('assets/front_end/assets/images/wind.svg') }}"><span>
                                        {{ $weather_result[0]->Wind->Speed->Metric->Value }}km/h </span>
                                </li>
                                <li><img src="{{ url('assets/front_end/assets/images/sunny-day.svg') }}"><span>
                                        UV-{{ $weather_result[0]->UVIndex }}
                                    </span>
                                </li>
                                @if ($weather_result[0]->HasPrecipitation)
                                    <li><img src="{{ url('assets/front_end/assets/images/precipitation.svg') }}"><span>
                                            {{ $weather_result[0]->Precip1hr->Metric->Value }}mm
                                        </span></li>
                                @endif

                            </ul>

                        </div> --}}
                    </div>
                    <div class="voteWrap">
                        <div class="voteWeatherHeader"> Vote for Weather </div>
                        <div class="voteSectionButton">
                            <button class="tablink activeVote" onclick="voteWeather(event,'viewVotes')">View Votes</button>
                            <button class="tablink" onclick="voteWeather(event,'tomorrow')">Tomorrow</button>
                            <button class="tablink" onclick="voteWeather(event,'dat')">Day After Tomorrow</button>
                        </div>
                        <div class="voteContentWrapper">
                            <div id="viewVotes" class="weekday">
                                <div class="voteHeadSeeAllWrap">
                                    <div class="voteTabHead">
                                        Today voted Temprature
                                    </div>
                                    <div class="seeall">
                                        See all
                                    </div>
                                </div>
                                <div class="personVoteWrap">
                                    <div class="personVote">
                                        <div class="personName">David </div>
                                        <div class="personGuessTempReportWrap">
                                            <span class="highTempSpan">High Temp - 48 </span>
                                            <span class="weatherSpan"> Rain </span>
                                        </div>
                                    </div>
                                    <div class="weatherImage">
                                        <img src="{{ url('assets/front_end/assets/images/voteCloud.svg') }}">
                                    </div>
                                </div>
                                <div class="personVoteWrap">
                                    <div class="personVote">
                                        <div class="personName">Chritsian </div>
                                        <div class="personGuessTempReportWrap">
                                            <span class="highTempSpan">High Temp - 48 </span>
                                            <span class="weatherSpan"> Snow </span>
                                        </div>
                                    </div>
                                    <div class="weatherImage">
                                        <img src="{{ url('assets/front_end/assets/images/voteSnowflake.svg') }}">
                                    </div>
                                </div>
                                <div class="personVoteWrap">
                                    <div class="personVote">
                                        <div class="personName">David </div>
                                        <div class="personGuessTempReportWrap">
                                            <span class="highTempSpan">High Temp - 48 </span>
                                            <span class="weatherSpan"> Ice </span>
                                        </div>
                                    </div>
                                    <div class="weatherImage">
                                        <img src="{{ url('assets/front_end/assets/images/voteCloud.svg') }}">
                                    </div>
                                </div>
                            </div>

                            <div id="tomorrow" class="weekday" style="display:none">
                                <div class="voteTomorrowWrapper">
                                    <div class="voteHighLowWrapper">
                                        Select High or Low Temprature
                                    </div>
                                </div>
                                <div class="alert alert-danger custom_alert" style="display: none;">
                                    <span class="alert_message"></span>
                                </div>
                                @livewire('weathervote')
                                {{-- <div class="radioButtonHighLowWrap">
                                    <form method="Post" action="{{ route('weathervote') }}">
                                        @csrf
                                        <div class="radioButtonInline">
                                            <div class="imagePdiv">
                                                <label>
                                                    <input type="radio" name="temp" value="0" checked="">
                                                    <div class="delOpt"> Low Temprature</div>
                                                </label>
                                            </div>

                                            <div class="imagePdiv">
                                                <label>
                                                    <input type="radio" name="temp" value="1">
                                                    <div class="delOpt"> High Temprature </div>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="addVoteWrapper">
                                            <div class="addVoteInput">
                                                <div class="avisHead"> Enter temp. value </div>
                                                <input type="text" id="tempenter" name="temp_value" placeholder="Hint :28">
                                            </div>
                                            <div class="addVoteSelect">
                                                <div class="avisHead"> Select list </div>
                                                <select name="precipitation" id="speed">
                                                    @foreach ($precipitation as $val)
                                                        <option value="{{ $val->id }}">
                                                            {{ $val->precipitation_name }}
                                                        </option>
                                                    @endforeach


                                                </select>
                                            </div>
                                        </div>
                                        <div class="voteSubmitDiv">
                                            <button class="voteSubmit"> Submit </button>
                                        </div>
                                    </form>
                                </div> --}}
                            </div>

                            <div id="dat" class="weekday" style="display:none">
                                <div class="voteTomorrowWrapper">
                                    <div class="voteHighLowWrapper">
                                        Select High or Low Temprature
                                    </div>
                                </div>
                                @livewire('weathervotenextday')
                                {{-- <div class="radioButtonHighLowWrap">
                                    <form action="#">
                                        <div class="radioButtonInline">
                                            <div class="imagePdiv">
                                                <label>
                                                    <input type="radio" name="test" value="editCS1" checked="">
                                                    <div class="delOpt"> Low Temprature</div>
                                                </label>
                                            </div>

                                            <div class="imagePdiv">
                                                <label>
                                                    <input type="radio" name="test" value="editCS2">
                                                    <div class="delOpt"> High Temprature </div>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="addVoteWrapper">
                                            <div class="addVoteInput">
                                                <div class="avisHead"> Enter temp. value </div>
                                                <input type="text" id="tempenter" name="temp" placeholder="Hint :28">
                                            </div>
                                            <div class="addVoteSelect">
                                                <div class="avisHead"> Select list </div>
                                                <select name="speed" id="speed">
                                                    <option value="noprecipitation">No precipitation </option>
                                                    <option value="rain">Rain</option>
                                                    <option value="snow">Snow</option>
                                                    <option value="ice">Ice</option>
                                                    <option value="mixed">Mixed</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="voteSubmitDiv">
                                            <button class="voteSubmit"> Submit </button>
                                        </div>
                                    </form>
                                </div> --}}
                            </div>

                        </div>
                    </div>

                </div>

            </div>

            <!-- Middle Section End -->

            <!-- Right Sidebar Start -->

            <div class="lg:w-5/12">

                <div class="bg-white shadow-md rounded-md overflow-hidden">

                    <div
                        class="bg-gray-50 dark:bg-gray-800 border-b border-gray-100 flex items-baseline justify-between py-4 px-6 dark:border-gray-800">
                        <h2 class="font-semibold text-lg">Top 5 Metrologist</h2>
                        <a href="weathermetrologist.html"> See All</a>
                    </div>

                    <div
                        class="divide-gray-300 divide-gray-50 divide-opacity-50 divide-y px-4 dark:divide-gray-800 dark:text-gray-100">
                        <div class="flex items-center justify-between py-3">
                            <div class="flex flex-1 items-center space-x-4">
                                <a href="profile.html">
                                    <img src="{{ url('assets/front_end/assets/images/avatars/avatar-2.jpg') }}"
                                        class="bg-gray-200 rounded-full w-10 h-10">
                                </a>
                                <div class="flex flex-col">
                                    <a href="metrologistProfile.html">
                                        <span class="block capitalize font-semibold"> Johnson smith </span>
                                        <span class="block capitalize text-sm"> Spain(Barcelona) </span>
                                    </a>
                                </div>
                            </div>

                            <a href="#" class="followButton"> Follow </a>
                        </div>
                        <div class="flex items-center justify-between py-3">
                            <div class="flex flex-1 items-center space-x-4">
                                <a href="profile.html">
                                    <img src="{{ url('assets/front_end/assets/images/avatars/avatar-1.jpg') }}"
                                        class="bg-gray-200 rounded-full w-10 h-10">
                                </a>
                                <div class="flex flex-col">
                                    <a href="metrologistProfile.html">
                                        <span class="block capitalize font-semibold"> James Lewis </span>
                                        <span class="block capitalize text-sm"> Spain(Barcelona) </span>
                                    </a>
                                </div>
                            </div>
                            <a href="#" class="followButton"> Follow </a>
                        </div>
                        <div class="flex items-center justify-between py-3">
                            <div class="flex flex-1 items-center space-x-4">
                                <a href="profile.html">
                                    <img src="{{ url('assets/front_end/assets/images/avatars/avatar-5.jpg') }}"
                                        class="bg-gray-200 rounded-full w-10 h-10">
                                </a>
                                <div class="flex flex-col">
                                    <a href="metrologistProfile.html">
                                        <span class="block capitalize font-semibold"> John Michael </span>
                                        <span class="block capitalize text-sm"> Spain(Barcelona) </span>
                                    </a>
                                </div>
                            </div>
                            <a href="#" class="followButton"> Follow </a>
                        </div>
                        <div class="flex items-center justify-between py-3">
                            <div class="flex flex-1 items-center space-x-4">
                                <a href="profile.html">
                                    <img src="{{ url('assets/front_end/assets/images/avatars/avatar-7.jpg') }}"
                                        class="bg-gray-200 rounded-full w-10 h-10">
                                </a>
                                <div class="flex flex-col">
                                    <a href="metrologistProfile.html">
                                        <span class="block capitalize font-semibold"> Monroe Parker </span>
                                        <span class="block capitalize text-sm"> Spain(Barcelona) </span>
                                    </a>
                                </div>
                            </div>

                            <a href="#" class="followButton"> Follow </a>
                        </div>

                        <div class="flex items-center justify-between py-3">
                            <div class="flex flex-1 items-center space-x-4">
                                <a href="profile.html">
                                    <img src="{{ url('assets/front_end/assets/images/avatars/avatar-3.jpg') }}"
                                        class="bg-gray-200 rounded-full w-10 h-10">
                                </a>
                                <div class="flex flex-col">
                                    <a href="metrologistProfile.html">
                                        <span class="block capitalize font-semibold"> David Warner </span>
                                        <span class="block capitalize text-sm"> Spain(Barcelona) </span>
                                    </a>
                                </div>
                            </div>

                            <a href="#" class="followButton"> Follow </a>
                        </div>

                    </div>

                </div>

                <div class="mt-5">
                    <div class="bg-white overflow-hidden">
                        <div class="bg-gray-50 border-b py-4 px-6">
                            <h2 class="font-semibold text-lg">Mayor Of the Barcelona</h2>
                        </div>
                        <div class="mayorWrap">
                            <span class="mayorImgCrownWrap">
                                <a href="mayorProfile.html">
                                    <img src="{{ url('assets/front_end/assets/images/avatars/avatar-8.jpg') }}"
                                        class="mayorImage bg-gray-200 rounded-full w-10 h-10">
                                </a>
                                <img class="mayorCrown" src="{{ url('assets/front_end/assets/images/crown.svg') }}">
                            </span>
                        </div>
                        <div class="mayorNamecityWrap">
                            <span class="mayorName"> Alfredo </span> is the Mayor of the <span
                                class="mayorCity"> Barcelona City </span>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Right Sidebar End -->

        </div>

    </div>

    <script type="text/javascript">
        if (navigator.geolocation) {
            var APP_URL = {!! json_encode(url('/')) !!}
            navigator.geolocation.getCurrentPosition(function(position) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: APP_URL + "/latlong",
                    data: {
                        lat: position.coords.latitude,
                        long: position.coords.longitude
                    },
                    success: function(data) {
                        $("#weathertemp").html(data);
                    }
                });
            });
        }
    </script>

@endsection
