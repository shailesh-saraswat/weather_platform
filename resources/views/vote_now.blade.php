@extends('layouts/front')
@section('pagetitle')
    Vote Now
@stop
@section('content')
    <section class="banner-1">
        <div class="container">
            <div class="row">
                <div class="search-icon">
                    <div class="two-grid">
                        <div class="temp">
                            <h2>28<sup>0</sup></h2>
                        </div>
                        <div class="temp-icon">
                            <img src="{{ url('assets/front/images/Partially_cloud.svg') }}">
                        </div>
                    </div>
                    <form>
                        <input type="search" placeholder="Search Location"><i class="fas fa-search"></i>
                    </form>
                    <div class="banner-con">
                        <div class="ww-details">

                            <div class="three-grid">
                                <div class="three-column">
                                    <h3>13km/h </h3>
                                    <img src="{{ url('assets/front/images/snowflake.svg') }}">
                                </div>
                                <div class="three-column">
                                    <h3>UV-3 </h3>
                                    <img src="{{ url('assets/front/images/sunny_day.svg') }}">
                                </div>
                                <div class="three-column">
                                    <h3>10 </h3>
                                    <img src="{{ url('assets/front/images/wind.svg') }}">
                                </div>
                                <div class="three-column">
                                    <h3>10 </h3>
                                    <img src="{{ url('assets/front/images/precipitation.svg') }}">
                                </div>
                                <div class="three-column">
                                    <h3>10 </h3>
                                    <img src="{{ url('assets/front/images/snowflake.svg') }}">
                                </div>
                                <div class="three-column">
                                    <h3>10 </h3>
                                    <img src="{{ url('assets/front/images/snowflake.svg') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container wow  fadeInUp p0 animated votesDivWrap" data-wow-delay="0.2s"
        style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
        <div class="row">
            <div class="all-container col-12">
                <div class="eight-container">

                    <div class="one-grid-container">



                        <div class="votes-now">
                            <div class="newheading">
                                <h2 class="mb20">VOTE NOW</h2>
                            </div>

                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="pill" href="#home">Today</a></li>
                                <li><a data-toggle="pill" href="#menu1">Tomorrow</a></li>
                                <li><a data-toggle="pill" href="#menu2">Day After Tomorrow</a></li>
                            </ul>
                            <form>
                                <div class="tab-content">

                                    <div id="home" class="tab-pane fade in active">
                                        <h3>Today Voted Temprature <a href="">See all</a></h3>
                                        <div class="user-voted">
                                            <h4>David<span>High Temp - 48 | 32 Votes</span></h4>
                                        </div>
                                        <div class="user-voted">
                                            <h4>David<span>High Temp - 48 | 32 Votes</span></h4>
                                        </div>
                                        <div class="user-voted">
                                            <h4>David<span>High Temp - 48 | 32 Votes</span></h4>
                                        </div>
                                        <div class="user-voted">
                                            <h4>David<span>High Temp - 48 | 32 Votes</span></h4>
                                        </div>
                                    </div>
                                    <div id="menu1" class="tab-pane fade">
                                        <h3>Select High or Low Temprature</h3>
                                        <div class="enter-temp">
                                            <div class="form-group">
                                                <label> Select list</label>
                                                <select>
                                                    <option>Low Temprature</option>
                                                    <option>High Temprature</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label> Enter temp. value</label>
                                                <input type="text" placeholder="Hint : 28">
                                            </div>
                                            <div class="form-group">
                                                <label> Select list</label>
                                                <select>
                                                    <option>No precipitation</option>
                                                    <option>Rain</option>
                                                    <option>Snow</option>
                                                    <option>Ice</option>
                                                    <option>Mixed</option>
                                                </select>
                                            </div>
                                            <button type="submit" class="enter-temp-bt">Submit</button>

                                        </div>

                                    </div>
                                    <div id="menu2" class="tab-pane fade">
                                        <h3>Select High or Low Temprature</h3>
                                        <div class="enter-temp">
                                            <div class="form-group">
                                                <label> Select list</label>
                                                <select>
                                                    <option>Low Temprature</option>
                                                    <option>High Temprature</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label> Enter temp. value</label>
                                                <input type="text" placeholder="Hint : 28">
                                            </div>
                                            <div class="form-group">
                                                <label> Select list</label>
                                                <select>
                                                    <option>No precipitation</option>
                                                    <option>Rain</option>
                                                    <option>Snow</option>
                                                    <option>Ice</option>
                                                    <option>Mixed</option>
                                                </select>
                                            </div>
                                        </div>
                                        <button type="submit" class="enter-temp-bt">Submit</button>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div style="clear:both;"></div>
@endsection
