<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>@yield('pagetitle')</title>
    <link href="{{ url('assets/front/css/css-bundle.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ url('assets/front/css/main-purple.css') }}" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link href="{{ url('assets/front/css/responsive.css') }}" type="text/css" rel="stylesheet">
    @livewireStyles
</head>

<body id="top" class="dark-theme" data-spy="scroll" data-target="#main-menu" data-offset="50">
    <a class="ease" href="#top" id="scrollTopBtn"><i class="fas fa-angle-up"></i></a>

    <div id="preloader" class="tlp-preloader">
        <div class="animation-preloader">
            <div class="tlp-spinner"></div>
            <img src="{{ url('assets/front/images/preloader.png') }}" alt="Preloader">
        </div>
    </div>
    @if (request()->is('/'))
        <header class="header header--transparent">
        @else
            <header class="header header--transparent inside-header">
    @endif
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-lg-2 col-6">
                <div class="header__logo header__logo--small">
                    <a href="{{ route('frontHome') }}">
                        @if (request()->is('/'))
                            <img src="{{ url('assets/front/images/logo3.svg') }}" class="origLogo">
                            <img src="{{ url('assets/front/images/logo2.svg') }}" class="logoadd">
                        @else
                            <img src="{{ url('assets/front/images/logo2.svg') }}" class="">
                        @endif

                    </a>
                </div>
            </div>
            <div class="col-lg-10 col-6">
                <nav id="main-menu" class="header__nav">
                    <ul class="header__menu">
                        <li class="header__item ">
                            <a href="{{ route('frontHome') }}"
                                class="ease nav-link header__link has_sub_menu">Home</a>

                        </li>
                        <li class="header__item"><a href="{{ route('aboutUs') }}"
                                class="ease nav-link header__link">About Us</a></li>
                        <li class="header__item"><a href="{{ route('Gallery') }}"
                                class="ease nav-link header__link">Gallery</a></li>
                        <li class="header__item"><a href="{{ route('Vote') }}"
                                class="ease nav-link header__link">Vote Now</a></li>
                        <li class="header__item"><a href="#" class="ease nav-link header__link">Contest</a></li>
                        <li class="header__item"><a href="{{ route('contactUs') }}"
                                class="ease nav-link header__link">Contact</a></li>
                    </ul>
                    @if (Auth::check())
                        <div class="header__addons noti-cont">
                            <ul class="header__menu">
                                <li class="header__item ">
                                    <a href="#top" class="ease nav-link header__link has_sub_menu">
                                        <img src="{{ url('assets/front/images/account.svg') }}"
                                            class="ac">
                                    </a>
                                    <ul class="sub_menu">

                                        <li class="nav-item">
                                            <a href="{{ route('userDashboard') }}" class="nav-link"> User
                                                Profile</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{ route('userLogout') }}" class="nav-link"> Log Out</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </div>
                    @else
                        <div class="header__addons noti-cont">
                            <ul class="header__menu">
                                <li class="header__item ">
                                    <a href="#top" class="ease nav-link header__link has_sub_menu">
                                        <img src="{{ url('assets/front/images/account.svg') }}"
                                            class="ac">
                                    </a>
                                    <ul class="sub_menu">
                                        <li class="nav-item">
                                            <a href="{{ route('Login') }}" class="nav-link">Login</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{ route('Register') }}" class="nav-link"
                                                href="">Signup</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </div>
                    @endif


                </nav>
                <button type="button" class="mobile-menu__btn"><span></span></button>
            </div>
        </div>
    </div>
    </header>
    <!-- ==================== End Header ==================== -->

    <!-- ==================== start mobile menu ==================== -->
    <div class="mobile-menu-overlay"></div>
    <div class="mobile-menu">
        <div class="mobile-menu__content">
            <ul class="mobile-menu__nav">
                <li class="mobile-menu__item"><a href="{{ route('frontHome') }}"
                        class="mobile-menu__link ease">Home</a></li>
                <li class="mobile-menu__item"><a href="{{ route('aboutUs') }}" class="mobile-menu__link ease">About
                        Us</a></li>
                <li class="mobile-menu__item"><a href="{{ route('Gallery') }}"
                        class="mobile-menu__link ease ">Gallery</a></li>
                <li class="mobile-menu__item"><a href="{{ route('Vote') }}" class="mobile-menu__link ease ">Vote
                        Now</a></li>
                <li class="mobile-menu__item"><a href="{{ route('aboutUs') }}"
                        class="mobile-menu__link ease">Contest</a></li>
                <li class="mobile-menu__item"><a href="{{ route('Login') }}" class="mobile-menu__link ease">Login</a>
                </li>
                <li class="mobile-menu__item"><a href="{{ route('Register') }}"
                        class="mobile-menu__link ease">Signup</a></li>
            </ul>
            <div class="mobile-menu__addons">
                <ul class="social-list">
                    <li class="social-list__item"><a href="#" class="social-list__link"><i
                                class="fab fa-facebook-f"></i></a></li>
                    <li class="social-list__item"><a href="#" class="social-list__link"><i
                                class="fab fa-twitter"></i></a></li>
                    <li class="social-list__item"><a href="#" class="social-list__link"><i
                                class="fab fa-instagram"></i></a></li>
                    <li class="social-list__item"><a href="#" class="social-list__link"><i
                                class="fab fa-linkedin-in"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
