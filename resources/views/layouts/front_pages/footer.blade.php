<footer class="footer footer--two footer--dark">
    <div class="container">
        <div class="row justify-content-between footer__top">
            <div class="col-12 col-lg-4">
                <div class="footer__contact footer__widget">
                    <div class="contacts_wrap">
                        <h5 class="footer__widget-title">About</h5>
                        <div class="footer__bio">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting has been the industry's
                                standard dummy text ever since Lorem Ipsum is simply dummy text of the printing and
                                typesetting has been the industry's standard dummy text ever since</p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-2">
                <div class="footer__widget">
                    <h5 class="footer__widget-title">Quick Links</h5>
                    <ul class="footer__list">
                        <li class="footer__list-item"><a href="{{ route('aboutUs') }}"
                                class="footer__list-link">About</a></li>
                        <li class="footer__list-item"><a href="{{ route('Gallery') }}"
                                class="footer__list-link">Gallery</a></li>
                        <li class="footer__list-item"><a href="{{ route('Vote') }}" class="footer__list-link">Vote
                                Now</a></li>
                        <!-- <li class="footer__list-item"><a href="{{ route('Register') }}" class="footer__list-link">Meterologists</a></li> -->
                        <li class="footer__list-item"><a href="{{ route('contactUs') }}"
                                class="footer__list-link">Contact Us</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-12 col-lg-2">
                <div class="footer__widget">
                    <h5 class="footer__widget-title">Service</h5>
                    <ul class="footer__list">
                        <!-- <li class="footer__list-item"><a href="#" class="footer__list-link">Blog</a></li> -->
                        <li class="footer__list-item"><a href="{{ route('termsConditions') }}"
                                class="footer__list-link">Terms and Condition</a></li>
                        <!-- <li class="footer__list-item"><a href="#" class="footer__list-link">Faq's</a></li> -->
                        <li class="footer__list-item"><a href="{{ route('privacyPolicy') }}"
                                class="footer__list-link">Privacy Policy</a></li>


                    </ul>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="footer__contact footer__widget">
                    <div class="contacts_wrap">
                        <h5 class="footer__widget-title">Contact Info</h5>

                        <ul class="footer__info info-list">
                            <li class="info-list__item">
                                <i class="info-list__icon fas fa-map-marker"></i>
                                <span class="info-list__data">New York, NY 5000, United States</span>
                            </li>
                            <li class="info-list__item">
                                <i class="info-list__icon  fas fa-phone"></i>
                                <span class="info-list__data"><a href="tel:123456789">1-234-567-8900</a></span>
                            </li>
                            <li class="info-list__item">
                                <i class="info-list__icon  fas fa-map-marker"></i>
                                <span class="info-list__data"><a
                                        href="mailto:contact@example.com">contact@example.com</a></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__copyright">
            <div class="row justify-content-between">
                <div class="col-12 col-lg-6">
                    <div class="footer__copy">{{ date('Y') }} ©&nbsp;<a href="#">Weather</a>. All rights reserved.
                    </div>
                </div>
                <div class="footer__social">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="{{ url('assets/front/js/js-bundle.js') }}"></script>
<script src="{{ url('assets/front/js/main.js') }}"></script>
<script src="{{ url('assets/front/js/custom_script.js') }}"></script>


<script>
    $(document).ready(function() {
        $("#myBtn").click(function() {
            $("#myModal").modal();
        });
    });
</script>
<script>
    $(function() {
        var header = 300;
        $(window).scroll(function() {
            var scroll = getCurrentScroll();
            if (scroll >= header) {
                $('.logoadd').addClass('showw');
                $('.origLogo').addClass('origShow');
            } else {
                $('.logoadd').removeClass('showw');
                $('.origLogo').removeClass('origShow');
            }
        });

        function getCurrentScroll() {
            return window.pageYOffset;
        }
    });
</script>
</body>

</html>
