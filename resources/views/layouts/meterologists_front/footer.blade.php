</div>

	</div>


	<script>
		function voteWeather(evt, tabName) {
		  var i, x, tablinks;
		  x = document.getElementsByClassName("weekday");
		  for (i = 0; i < x.length; i++) {
			x[i].style.display = "none";
		  }
		  tablinks = document.getElementsByClassName("tablink");
		  for (i = 0; i < x.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" activeVote", "");
		  }
		  document.getElementById(tabName).style.display = "block";
		  evt.currentTarget.className += " activeVote";
		}
	</script>

    <script src="{{url('assets/front_end/assets/js/tippy.all.min.js')}}"></script>  
    <script src="{{url('assets/front_end/assets/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{url('assets/front_end/assets/js/uikit.js')}}"></script>
    <script src="{{url('assets/front_end/assets/js/simplebar.js')}}"></script>
    <script src="{{url('assets/front_end/assets/js/custom.js')}}"></script>
    <script src="../../unpkg.com/ionicons%405.2.3/dist/ionicons.js')}}"></script>
</body>
</html>