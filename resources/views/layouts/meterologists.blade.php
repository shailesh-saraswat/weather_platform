@include('layouts.meterologists_front.header')
@yield('content')
@include('layouts.meterologists_front.footer')
@yield('js')