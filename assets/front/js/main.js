"use strict";
 

$(window).on('load', function () {
	 /*-------------------------------------
  Preloader
  -------------------------------------*/
  $('#preloader').fadeOut('slow', function() {
    $(this).remove();
  });	
  
  $('.preloader').delay(500).fadeOut('slow');
});
$(document).ready(function () {
  /* ------------------------------------- */

  /* *.Splitting .......................... */

  /* ------------------------------------- */
  Splitting();
  /* ------------------------------------- */

  /* *.WOW JS .......................... */

  /* ------------------------------------- */

  var wow = new WOW({
    animateClass: 'animated',
    offset: 10,
    mobile: true
  });
  wow.init();
  /* ------------------------------------- */

  /* *.Parallax .......................... */

  /* ------------------------------------- */

  $('.jarallax').jarallax({
    speed: 0.6
  });
  /* ------------------------------------- */

  /* *. Smooth Scroll To Anchor.......................... */

  /* ------------------------------------- */

  $('a.ease[href^="#"]').on('click', function (event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: $($anchor.attr('href')).offset().top
    }, 1500, 'easeInOutExpo');
    event.preventDefault();
  });
  /* ------------------------------------- */

  /* *.Sticky Header & Back to top ........ */

  /* ------------------------------------- */

  $(window).on('scroll', function () {
    if ($(window).width() > 992) {
      if ($(window).scrollTop() < 200) {
        $(".header").removeClass('header--sticky');
        $('#scrollTopBtn').removeClass('active');
      } else {
        $(".header").addClass('header--sticky');
        $('#scrollTopBtn').addClass('active');
      }
    }
  });
  /* ------------------------------------- */

  /* *. Mobile Menu.......................... */

  /* ------------------------------------- */

  $('.mobile-menu__btn').on('click', function (event) {
    event.preventDefault();
    $(this).toggleClass('active');
    $('.mobile-menu-overlay').toggleClass('active');
    $('.mobile-menu').toggleClass('active');
  });
  $('.mobile-menu-overlay').on('click', function (event) {
    event.preventDefault();
    $(this).toggleClass('active');
    $('.mobile-menu__btn').toggleClass('active');
    $('.mobile-menu').toggleClass('active');
  });
   

  $(".testimonial-one-carousel").owlCarousel({
    loop: true,
    autoplay: true,
    smartSpeed: 450,
    autoplayHoverPause: true,
    dots: true,
    nav: false,
    responsiveClass: true,
    items: 1
  });
   

  $('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: '.grid-sizer',
    percentPosition: true
  });
   
   $(document).ready(function(){
$('.image-popup-vertical-fit').magnificPopup({
	type: 'image',
  mainClass: 'mfp-with-zoom', 
  gallery:{
			enabled:true
		},

  zoom: {
    enabled: true, 

    duration: 300, // duration of the effect, in milliseconds
    easing: 'ease-in-out', // CSS transition easing function

    opener: function(openerElement) {

      return openerElement.is('img') ? openerElement : openerElement.find('img');
  }
}

});

});
});
